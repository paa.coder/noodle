package io.gitlab.paa.coder.noodlecriteriaapi;

import jakarta.persistence.Tuple;
import jakarta.persistence.criteria.*;

import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;

public interface Noodle {


    CriteriaBuilder criteriaBuilder();

    default NoodleScanner.SpecificationConverter specificationConverter(){
        return new NoodleScanner.SpecificationConverter() {};
    }

    default <T> SelectQuery<T> from(Class<T> entity){
        return new SelectQuery.Sample<T>(entity,scannerBuilder());
    }

    default <T> UpdateQuery<T> update(Class<T> entity){
        return new UpdateQuery.Sample<T>(entity, scannerBuilder());
    }



    default <T> DeleteQuery<T> delete(Class<T> entity){
        return new DeleteQuery.Sample<T>(entity,scannerBuilder());
    }

    default NoodleScanner.Builder scannerBuilder(){
        return new NoodleScanner.Builder() {
            @Override
            public NoodleScanner.SpecificationConverter specificationConverter(){
                return Noodle.this.specificationConverter();
            }

            @Override
            public CriteriaBuilder criteriaBuilder(){
                return Noodle.this.criteriaBuilder();
            }
        };
    }

    interface QueryFactory<T> {

        default CriteriaQuery<T> query(){
            return query(null);
        }

        CriteriaQuery<T> query(String alias);

        CriteriaQuery<Long> count();

        WhereClause specification();
    }

    interface SelectQuery<T> extends QueryFactory<T> {

        Class<T> entity();

        boolean distinct();

        SelectQuery<T> distinct(boolean distinct);

        SelectQuery<T> where(Function<WhereClause.InitChain,WhereClause.Factory> function);

        SelectQuery<T> order(Consumer<OrderClause.Factory> consumer);

        MultiSelectQuery<T> select(Consumer<SelectClause.Registry> consumer);

        default MultiSelectQuery<T> group(Consumer<SelectClause.Registry> consumer){
            return group(consumer, false);
        }

        MultiSelectQuery<T> group(Consumer<SelectClause.Registry> consumer, boolean addToSelect);

        record Sample<T>(Class<T> entity, WhereClause.Registry whereRegistry, NoodleScanner.Builder scannerBuilder, OrderClause.Registry orderRegistry,
                         AtomicBoolean distinctRegistry) implements SelectQuery<T> {

            public Sample(Class<T> entity, NoodleScanner.Builder scannerBuilder){
                this(entity,
                     new WhereClause.Registry(new ArrayList<>(), Predicate.BooleanOperator.AND, false),
                     scannerBuilder,
                     new OrderClause.Registry(),
                     new AtomicBoolean(false));
            }

            @Override
            public WhereClause specification(){
                return whereRegistry();
            }
            private void setClauses(NoodleScanner scanner, CriteriaQuery<?> cr){
                Optional.ofNullable(whereRegistry.generate(scanner)).ifPresent(cr::where);
                cr.distinct(distinctRegistry().get());
            }

            @Override
            public CriteriaQuery<T> query(String alias){

                CriteriaQuery<T> cr = scannerBuilder().criteriaBuilder().createQuery(entity);
                Root<T> root = cr.from(entity());

                final NoodleScanner scanner = scannerBuilder().build(root, cr);
                setClauses(scanner, cr);

                cr.select(root);
                Optional.of(orderRegistry.generate(scanner)).filter(i -> i.length > 0).ifPresent(cr::orderBy);


                return cr;
            }

            @Override
            public SelectQuery<T> distinct(boolean distinct){
                distinctRegistry().set(distinct);
                return this;
            }

            @Override
            public boolean distinct(){
                return distinctRegistry().get();
            }

            @Override
            public SelectQuery<T> order(Consumer<OrderClause.Factory> consumer){
                consumer.accept(orderRegistry());
                return this;
            }

            @Override
            public SelectQuery<T> where(Function<WhereClause.InitChain,WhereClause.Factory> function){
                whereRegistry().and().clause(function);
                return this;
            }

            @Override
            public MultiSelectQuery<T> group(Consumer<SelectClause.Registry> consumer, boolean addToSelect){
                return new MultiSelectQuery.Sample<>(this).group(consumer, addToSelect);
            }

            @Override
            public MultiSelectQuery<T> select(Consumer<SelectClause.Registry> consumer){
                return new MultiSelectQuery.Sample<>(this).select(consumer);
            }

            @Override
            public CriteriaQuery<Long> count(){
                CriteriaQuery<Long> cr = scannerBuilder().criteriaBuilder().createQuery(Long.class);
                Root<T> root = cr.from(entity());

                final NoodleScanner scanner = scannerBuilder().build(root, cr);
                setClauses(scanner, cr);
                cr.select(scanner.criteriaBuilder().count(root));

                return cr;
            }
        }
    }

    interface MultiSelectQuery<T> extends QueryFactory<Tuple> {
        Class<T> entity();

        MultiSelectQuery<T> where(Function<WhereClause.InitChain,WhereClause.Factory> function);

        MultiSelectQuery<T> select(Consumer<SelectClause.Registry> consumer);

        default MultiSelectQuery<T> group(Consumer<SelectClause.Registry> consumer){
            return group(consumer, false);
        }

        MultiSelectQuery<T> group(Consumer<SelectClause.Registry> consumer, boolean addToSelect);

        MultiSelectQuery<T> having(Function<WhereClause.InitChain,WhereClause.Factory> function);

        MultiSelectQuery<T> order(Consumer<OrderClause.Factory> consumer);

        boolean distinct();

        MultiSelectQuery<T> distinct(boolean distinct);

        record Sample<T>(Class<T> entity, NoodleScanner.Builder scannerBuilder, WhereClause.Registry whereRegistry, SelectClause.Registry.Impl selectRegistry,
                         SelectClause.Registry.Impl groupRegistry, WhereClause.Registry havingRegistry, OrderClause.Registry orderRegistry,
                         AtomicBoolean distinctRegistry) implements MultiSelectQuery<T> {
            public Sample(SelectQuery.Sample<T> selectQuery){
                this(selectQuery.entity(),
                     selectQuery.scannerBuilder(),
                     selectQuery.whereRegistry(),
                     new SelectClause.Registry.Impl(),
                     new SelectClause.Registry.Impl(),
                     new WhereClause.Registry(new ArrayList<>(), Predicate.BooleanOperator.AND, false),
                     selectQuery.orderRegistry(), selectQuery.distinctRegistry());
            }

            @Override
            public WhereClause specification(){
                return whereRegistry();
            }

            @Override
            public CriteriaQuery<Tuple> query(String alias){

                CriteriaQuery<Tuple> cr = scannerBuilder().criteriaBuilder().createTupleQuery();
                Root<T> root = cr.from(entity());

                final NoodleScanner scanner = scannerBuilder().build(root, cr);
                setClauses(scanner, cr);
                Optional.of(selectRegistry().generate(scanner)).filter(i -> i.length > 0).ifPresent(cr::multiselect);
                Optional.of(orderRegistry().generate(scanner)).filter(i -> i.length > 0).ifPresent(cr::orderBy);


                return cr;
            }

            @Override
            public CriteriaQuery<Long> count(){
                CriteriaQuery<Long> cr = scannerBuilder().criteriaBuilder().createQuery(Long.class);
                Root<T> root = cr.from(entity());

                final NoodleScanner scanner = scannerBuilder().build(root, cr);
                setClauses(scanner, cr);
                cr.select(scanner.criteriaBuilder().count(root));

                return cr;
            }


            @Override
            public MultiSelectQuery<T> distinct(boolean distinct){
                distinctRegistry().set(distinct);
                return this;
            }

            @Override
            public boolean distinct(){
                return distinctRegistry().get();
            }

            private void setClauses(NoodleScanner scanner, CriteriaQuery<?> cr){
                Optional.ofNullable(whereRegistry().generate(scanner)).ifPresent(cr::where);
                Optional.of(groupRegistry().generate(scanner)).filter(i -> i.length > 0).ifPresent(cr::groupBy);
                Optional.ofNullable(havingRegistry().generate(scanner)).ifPresent(cr::having);
                cr.distinct(distinctRegistry().get());

            }


            @Override
            public MultiSelectQuery<T> order(Consumer<OrderClause.Factory> consumer){
                consumer.accept(orderRegistry);
                return this;
            }

            @Override
            public MultiSelectQuery<T> where(Function<WhereClause.InitChain,WhereClause.Factory> function){
                whereRegistry().and().clause(function);
                return this;
            }

            @Override
            public MultiSelectQuery<T> select(Consumer<SelectClause.Registry> consumer){
                consumer.accept(selectRegistry);
                return this;
            }

            @Override
            public MultiSelectQuery<T> group(Consumer<SelectClause.Registry> consumer, boolean addToSelect){
                consumer.accept(groupRegistry());
                if(addToSelect){
                    consumer.accept(selectRegistry());
                }
                return this;
            }

            @Override
            public MultiSelectQuery<T> having(Function<WhereClause.InitChain,WhereClause.Factory> function){
                havingRegistry().and().clause(function);
                return this;
            }
        }
    }


    interface UpdateQuery<T>{

        Class<T> entity();

        UpdateQuery<T> where(Function<WhereClause.InitChain,WhereClause.Factory> function);

        UpdateQuery<T> set(Consumer<SetClause.Factory> function);

        CriteriaUpdate<T> update();

        record Sample<T>(Class<T> entity,NoodleScanner.Builder scannerBuilder, WhereClause.Registry whereRegistry, SetClause.Registry setRegistry) implements UpdateQuery<T> {

            public Sample(Class<T> entity, NoodleScanner.Builder scannerBuilder){
                this(entity,
                     scannerBuilder,
                     new WhereClause.Registry(new ArrayList<>(), Predicate.BooleanOperator.AND, false),
                     new SetClause.Registry());
            }


            @Override
            public UpdateQuery<T> set(Consumer<SetClause.Factory> function){
                function.accept(setRegistry());
                return this;
            }

            @Override
            public CriteriaUpdate<T> update(){

                CriteriaUpdate<T> cr = scannerBuilder().criteriaBuilder().createCriteriaUpdate(entity);
                Root<T> root = cr.from(entity());

                final NoodleScanner scanner = scannerBuilder().build(root, cr);

                Optional.ofNullable(whereRegistry().generate(scanner)).ifPresent(cr::where);
                setRegistry.apply(scanner,cr);


                return cr;
            }


            @Override
            public UpdateQuery<T> where(Function<WhereClause.InitChain,WhereClause.Factory> function){
                whereRegistry().and().clause(function);
                return this;
            }
        }
    }


    interface DeleteQuery<T>{

        Class<T> entity();

        DeleteQuery<T> where(Function<WhereClause.InitChain,WhereClause.Factory> function);

        CriteriaDelete<T> delete();

        record Sample<T>(Class<T> entity,NoodleScanner.Builder scannerBuilder, WhereClause.Registry whereRegistry) implements DeleteQuery<T> {

            public Sample(Class<T> entity, NoodleScanner.Builder scannerBuilder){
                this(entity,
                     scannerBuilder,
                     new WhereClause.Registry(new ArrayList<>(), Predicate.BooleanOperator.AND, false));
            }

            @Override
            public CriteriaDelete<T> delete(){

                CriteriaDelete<T> cr = scannerBuilder().criteriaBuilder().createCriteriaDelete(entity);
                Root<T> root = cr.from(entity());

                final NoodleScanner scanner = scannerBuilder().build(root, cr);
                Optional.ofNullable(whereRegistry().generate(scanner)).ifPresent(cr::where);

                return cr;
            }


            @Override
            public DeleteQuery<T> where(Function<WhereClause.InitChain,WhereClause.Factory> function){
                whereRegistry().and().clause(function);
                return this;
            }
        }
    }


    interface Generator<T> {
        T generate(NoodleScanner scanner);

    }

    interface Worker<T> {
        void apply(NoodleScanner scanner,T t);

    }

}
