package io.gitlab.paa.coder.noodlecriteriaapi.specifications;

import io.gitlab.paa.coder.noodlecriteriaapi.NoodleScanner;
import io.gitlab.paa.coder.noodlecriteriaapi.SelectClause;
import io.gitlab.paa.coder.noodlecriteriaapi.WhereClause;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Predicate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

public interface NoodleSpecification {

    Expression<?> left();

    Expression<?> right();

    String operator();

    Object value();

    record Impl(Expression<?> left, Expression<?> right, String operator, Object value) implements NoodleSpecification {}

    interface Criteria extends WhereClause {

        default SelectClause left(){
            return null;
        }

        default String operator(){
            return null;
        }

        default SelectClause right(){
            return null;
        }

        default Object value(){
            return null;
        }

        default Criteria withLeft(SelectClause left){
            return new Impl(left, operator(), right(), value());
        }

        default Criteria withOperator(String operator){
            return new Impl(left(), operator, right(), value());
        }

        default Criteria withRight(SelectClause right){
            return new Impl(left(), operator(), right, value());
        }

        default Criteria withValue(Object value){
            return new Impl(left(), operator(), right(), value);
        }

        @Override
        default Predicate generate(NoodleScanner scanner){
            AtomicReference<Predicate> predicate = new AtomicReference<>(null);

            final Iterator<Interceptor> iterator = scanner.specificationConverter().specificationFilters().iterator();

            final NoodleSpecification.Impl spec = new NoodleSpecification.Impl(Optional.ofNullable(left()).map(i -> i.generate(scanner)).orElse(null),
                                                                               Optional.ofNullable(right()).map(i -> i.generate(scanner)).orElse(null),
                                                                               operator(),
                                                                               value());
            while(predicate.get() == null && iterator.hasNext()){
                predicate.set(iterator.next().get(spec, scanner));
            }
            return predicate.get();
        }

        record Impl(SelectClause left, String operator, SelectClause right, Object value) implements Criteria {
        }
    }


    interface Interceptor {


        Predicate get(NoodleSpecification spec, NoodleScanner scanner);

        static List<Interceptor> defaultList(){
            final ArrayList<Interceptor> builders = new ArrayList<>();
            builders.add(new NULL() {});
            builders.add(new IN() {});
            builders.add(new EQUAL() {});
            builders.add(new LIKE() {});
            builders.add(new COMPARISON() {});
            return builders;
        }
    }


    interface NULL extends Interceptor {
        @Override
        default Predicate get(NoodleSpecification spec, NoodleScanner scanner){
            return switch(spec.operator().toUpperCase()){
                case "IS NULL" -> spec.left().isNull();
                case "IS NOT NULL" -> spec.left().isNull().not();
                default -> null;
            };
        }
    }
}
