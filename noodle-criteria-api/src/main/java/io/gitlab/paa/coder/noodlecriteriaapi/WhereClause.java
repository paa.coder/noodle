package io.gitlab.paa.coder.noodlecriteriaapi;

import io.gitlab.paa.coder.noodlecriteriaapi.specifications.NoodleSpecification;
import jakarta.persistence.criteria.Predicate;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface WhereClause extends Noodle.Generator<Predicate> {

    record Registry(Collection<WhereClause> store, Predicate.BooleanOperator operator, boolean isNot) implements Factory {
        @Override
        public Predicate generate(NoodleScanner scanner){

            final Predicate[] array = store().stream().map(i -> i.generate(scanner)).filter(Objects::nonNull).toArray(Predicate[]::new);
            if(array.length == 0){
                return null;
            }
            Predicate predicate = switch(operator()){
                case AND -> scanner.criteriaBuilder().and(array);
                case OR -> scanner.criteriaBuilder().or(array);
            };
            return isNot() ? predicate.not() : predicate;
        }

        public Registry create(Predicate.BooleanOperator operator, WhereClause... clause){
            return create(false, operator, clause);
        }

        public Registry create(boolean isNot, Predicate.BooleanOperator operator, WhereClause... clause){
            return new Registry(Stream.of(clause).collect(Collectors.toList()), operator, isNot);
        }

        @Override
        public Factory add(Predicate.BooleanOperator operator, WhereClause clause){
            if(Objects.equals(operator, operator())){
                store().add(clause);
                return this;
            }else{
                if(store().size() <= 1){
                    return create(operator, Stream.concat(store().stream(), Stream.of(clause)).toArray(WhereClause[]::new));
                }
                return create(operator, this, clause);
            }
        }

        @Override
        public InitChain chain(Predicate.BooleanOperator predicate){
            return new InitChain.Sample(this, predicate);
        }

        @Override
        public Factory not(){
            return new Registry(store(), operator(), ! isNot());
        }
    }

    interface Factory extends WhereClause {
        Factory add(Predicate.BooleanOperator operator, WhereClause clause);

        default InitChain and(){
            return chain(Predicate.BooleanOperator.AND);
        }

        default InitChain or(){
            return chain(Predicate.BooleanOperator.OR);
        }

        default OperatorChain and(String field){
            return chain(field,Predicate.BooleanOperator.AND);
        }

        default OperatorChain or(String field){
            return chain(field,Predicate.BooleanOperator.OR);
        }

        InitChain chain(Predicate.BooleanOperator predicate);

        default OperatorChain chain(String field,Predicate.BooleanOperator predicate){
            return chain(predicate).field(field);
        }

        Factory not();
    }

    interface CloseChain extends SelectClause.Factory<Factory> {
        Factory value(Object o);

        record Sample(Function<WhereClause,Factory> consumer, NoodleSpecification.Criteria criteria) implements CloseChain {

            @Override
            public Factory add(SelectClause selectClause){
                return consumer.apply(criteria().withRight(selectClause));
            }

            @Override
            public Factory value(Object o){
                if(o == null){
                    return literal(null);
                }
                return consumer.apply(criteria().withValue(o));
            }
        }
    }

    interface OperatorChain {
        CloseChain is(String operator);

        Factory add(String operator, Object value);

        default Factory isNull(){
            return add("is null", null);
        }

        default Factory isNotNull(){
            return add("is not null", null);
        }

        default Factory eq(Object value){
            return add("=", value);
        }

        default Factory ne(Object value){
            return add("<>", value);
        }

        default Factory lt(Object value){
            return add("<", value);
        }

        default Factory le(Object value){
            return add("<=", value);
        }

        default Factory gt(Object value){
            return add(">", value);
        }

        default Factory ge(Object value){
            return add(">=", value);
        }

        default Factory in(Collection<?> value){
            return add("in", value);
        }

        default Factory nin(Collection<?> value){
            return add("not in", value);
        }

        default Factory in(Object... value){
            return in(List.of(value));
        }

        default Factory nin(Object... value){
            return nin(List.of(value));
        }

        default Factory like(String value){
            return add("like", value);
        }

        default Factory ilike(String value){
            return add("ilike", value);
        }

        default CloseChain eq(){
            return is("=");
        }

        default CloseChain ne(){
            return is("<>");
        }

        default CloseChain lt(){
            return is("<");
        }

        default CloseChain le(){
            return is("<=");
        }

        default CloseChain gt(){
            return is(">");
        }

        default CloseChain ge(){
            return is(">=");
        }

        default CloseChain in(){
            return is("in");
        }

        default CloseChain nin(){
            return is("not in");
        }

        default CloseChain like(){
            return is("like");
        }

        default CloseChain ilike(){
            return is("ilike");
        }

        record Sample(Function<WhereClause,Factory> consumer, NoodleSpecification.Criteria criteria) implements OperatorChain {

            @Override
            public CloseChain is(String operator){
                return new CloseChain.Sample(consumer(), criteria().withOperator(operator));
            }

            @Override
            public Factory add(String operator, Object value){
                assert operator != null;
                return consumer.apply(criteria.withOperator(operator).withValue(value));
            }
        }
    }

    interface InitChain extends SelectClause.Factory<OperatorChain> {
        InitChain not();

        default Factory fieldEq(String field,Object value){
            return field(field).eq(value);
        }

        Factory add(WhereClause whereClause);

        <W> Factory exists(Class<W> tClass, String alias, Function<SelectClause.SubQuery<W,SelectClause.QueryClause>,SelectClause.QueryClause> function);

        Factory clause(Function<InitChain,Factory> function);

        record Sample(Registry factory, boolean isNot, Predicate.BooleanOperator operator) implements InitChain {
            public Sample(Registry factory, Predicate.BooleanOperator operator){
                this(factory, false, operator);
            }

            @Override
            public Factory clause(Function<InitChain,Factory> function){
                return add(function.apply(factory.create(isNot(), operator()).chain(operator())));
            }

            @Override
            public OperatorChain add(SelectClause selectClause){
                return new OperatorChain.Sample(this::add, new NoodleSpecification.Criteria() {}.withLeft(selectClause));
            }

            @Override
            public <W> Factory exists(Class<W> tClass, String alias, Function<SelectClause.SubQuery<W,SelectClause.QueryClause>,SelectClause.QueryClause> function){
                return add((WhereClause) scanner -> Optional
                        .ofNullable(function.apply(SelectClause.SubQuery.of(tClass, alias, s -> s)))
                        .map(i -> i.generate(scanner))
                        .map(expression -> scanner.criteriaBuilder().exists(expression))
                        .orElse(null));
            }

            @Override
            public InitChain not(){
                return new Sample(factory(), ! isNot(), operator());
            }

            @Override
            public Factory add(WhereClause whereClause){
                if(whereClause == null){
                    return factory();
                }
                return factory.add(operator(),
                                   isNot() ? (WhereClause) scanner -> Optional.ofNullable(whereClause.generate(scanner)).map(Predicate::not).orElse(null) : whereClause);
            }
        }
    }

    class SpecificationException extends RuntimeException {
        public SpecificationException(String message, Throwable cause){
            super(message, cause);
        }

        public SpecificationException(String message){
            super(message);
        }
    }


}
