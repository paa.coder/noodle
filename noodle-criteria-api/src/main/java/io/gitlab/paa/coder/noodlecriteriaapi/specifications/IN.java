package io.gitlab.paa.coder.noodlecriteriaapi.specifications;

import io.gitlab.paa.coder.noodlecriteriaapi.NoodleScanner;
import io.gitlab.paa.coder.noodlecriteriaapi.WhereClause;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Predicate;

import java.util.Collection;
import java.util.Objects;

public interface IN extends NoodleSpecification.Interceptor {
    @Override
    default Predicate get(NoodleSpecification spec, NoodleScanner scanner){
        if(spec.value()!=null){
            return new Val(spec,scanner).get();
        }else if(spec.right()!=null){
            return new Exp(spec,scanner).get();
        }
        return null;
    }

    interface Common {

        NoodleSpecification spec();

        NoodleScanner scanner();

        <T> CriteriaBuilder.In<T> setValues(CriteriaBuilder.In<T> in, Class<? extends T> tClass);

        default <T> CriteriaBuilder.In<T> in(Expression<T> expression){
            return setValues(scanner().criteriaBuilder().in(expression), expression.getJavaType());
        }

        default Predicate get(){
            return switch(spec().operator().toUpperCase()){
                case "IN" -> in(spec().left());
                case "NOT IN" -> in(spec().left()).not();
                default -> null;
            };
        }
    }

    record Val(NoodleSpecification spec,NoodleScanner scanner) implements IN.Common {
        @Override
        public  <T> CriteriaBuilder.In<T> setValues(CriteriaBuilder.In<T> in, Class<? extends T> tClass){
            if(spec().value() instanceof Collection<?> c){
                return c.stream().filter(Objects::nonNull).map(i -> {
                    if(tClass==null){
                        return (T)i;
                    }
                    try{
                        return scanner().specificationConverter().convert(i, tClass);
                    }catch(Throwable t){
                        throw new WhereClause.SpecificationException(String.format("error convert object %s to %s(by %s) in collection %s for criteria %s",
                                                                                   i,
                                                                                   tClass,
                                                                                   spec().left(),
                                                                                   c,
                                                                                   spec().operator()), t);
                    }
                }).reduce(in, CriteriaBuilder.In::value, (l, r) -> r);
            }

            final T convert;
            if(tClass==null){
                convert = (T) spec().value();
            }else {
                try{
                    convert = scanner().specificationConverter().convert(spec().value(), tClass);
                }catch(Throwable t){
                    throw new WhereClause.SpecificationException(String.format("error convert object %s to %s(by %s) for criteria %s",
                                                                               spec().value(),
                                                                               tClass,
                                                                               spec().left(),
                                                                               spec().operator()), t);
                }
            }


            return in.value(convert);
        }
    }

    record Exp(NoodleSpecification spec,NoodleScanner scanner) implements IN.Common {
        @Override
        public  <T> CriteriaBuilder.In<T> setValues(CriteriaBuilder.In<T> in, Class<? extends T> tClass){
            Expression<? extends T> exp;
            if(tClass != null){
                if(spec().right().getJavaType() != null && tClass.isAssignableFrom(spec().right().getJavaType())){
                    exp = (Expression<T>) spec().right();
                }else{
                    exp = spec().right().as(tClass);
                }
            }else{
                exp = (Expression<T>)spec().right();
            }

            return in.value(exp);
        }
    }
}