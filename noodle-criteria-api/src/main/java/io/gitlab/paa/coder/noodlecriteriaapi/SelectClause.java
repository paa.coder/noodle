package io.gitlab.paa.coder.noodlecriteriaapi;

import jakarta.persistence.criteria.*;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

public interface SelectClause extends Noodle.Generator<Expression<?>> {

    interface QueryClause extends SelectClause {
        @Override
        Subquery<?> generate(NoodleScanner scanner);
    }

    interface Factory<T> {

        T add(SelectClause selectClause);

        default T custom(SelectClause selectClause){
            return add(selectClause);
        }

        default T field(String field){
            return add((Column) () -> field);
        }

        default <W> T query(Class<W> tClass, String alias, Function<SubQuery<W,T>,T> function){
            return function.apply(SubQuery.of(tClass, alias, this::add));
        }

        default T literal(Object o){
            return add(scanner -> scanner.criteriaBuilder().literal(o));
        }

        default T literalAs(Object o, Class<?> clazz){
            return add(scanner -> scanner.criteriaBuilder().literal(scanner.specificationConverter().convert(o, clazz)));
        }

        default T cast(Object o, Class<?> tClass){
            return add(scanner -> scanner.criteriaBuilder().literal(o).as(tClass));
        }

        default T cast(Function<Factory<SelectClause>,SelectClause> function, Class<?> tClass){
            return add(scanner -> function.apply(clause()).generate(scanner).as(tClass));
        }

        default T _null(Class<?> clazz){
            return add(scanner -> scanner.criteriaBuilder().nullLiteral(clazz));
        }

        default T math(Function<Math,Math> function){
            return add(function.apply(new Math() {}));
        }

        default T coalesce(Function<Coalesce,Coalesce> function){
            return add(function.apply(new Coalesce() {}));
        }

        default T concat(Function<Concat,Concat> function){
            return add(function.apply(new Concat() {}));
        }

        default T lenght(Function<Factory<SelectClause>,SelectClause> function){
            return add(scanner -> {
                Expression<?> generate = function.apply(clause()).generate(scanner);
                if(! String.class.isAssignableFrom(generate.getJavaType())){
                    generate = generate.as(String.class);
                }
                return scanner.criteriaBuilder().length((Expression<String>) generate);
            });
        }

        default T avg(Function<Factory<SelectClause>,SelectClause> function){
            return add(scanner -> {
                Expression<?> generate = function.apply(clause()).generate(scanner);
                if(! Number.class.isAssignableFrom(generate.getJavaType())){
                    throw new IllegalArgumentException(String.format(
                            "java type of expression %s for avg() operation should аppointed from Number.class (current type %s)",
                            generate,
                            generate.getJavaType()));
                }
                return scanner.criteriaBuilder().avg((Expression<Number>) generate);
            });
        }

        default T sum(Function<Factory<SelectClause>,SelectClause> function){
            return add(scanner -> {
                Expression<?> generate = function.apply(clause()).generate(scanner);
                if(! Number.class.isAssignableFrom(generate.getJavaType())){
                    throw new IllegalArgumentException(String.format(
                            "java type of expression %s for sum() operation should аppointed from Number.class (current type %s)",
                            generate,
                            generate.getJavaType()));
                }
                return scanner.criteriaBuilder().sum((Expression<Number>) generate);
            });
        }


        default T max(Function<Factory<SelectClause>,SelectClause> function){
            return add(scanner -> {
                Expression<?> generate = function.apply(clause()).generate(scanner);

                if(Number.class.isAssignableFrom(generate.getJavaType())){
                    return scanner.criteriaBuilder().max((Expression<Number>) generate);
                }

                if(Comparable.class.isAssignableFrom(generate.getJavaType())){
                    return scanner.criteriaBuilder().greatest((Expression<Comparable>) generate);
                }
                throw new IllegalArgumentException(String.format(
                        "java type of expression %s for max() operation should аppointed from Number.class or Comparable.class (current type %s)",
                        generate,
                        generate.getJavaType()));
            });
        }

        default T min(Function<Factory<SelectClause>,SelectClause> function){
            return add(scanner -> {
                Expression<?> generate = function.apply(clause()).generate(scanner);

                if(Number.class.isAssignableFrom(generate.getJavaType())){
                    return scanner.criteriaBuilder().min((Expression<Number>) generate);
                }

                if(Comparable.class.isAssignableFrom(generate.getJavaType())){
                    return scanner.criteriaBuilder().least((Expression<Comparable>) generate);
                }
                throw new IllegalArgumentException(String.format(
                        "java type of expression %s for min() operation should аppointed from Number.class or Comparable.class (current type %s)",
                        generate,
                        generate.getJavaType()));
            });
        }

        default T count(Function<Factory<SelectClause>,SelectClause> function){
            return add(scanner -> scanner.criteriaBuilder().count(function.apply(clause()).generate(scanner)));
        }

        default T countDistinct(Function<Factory<SelectClause>,SelectClause> function){
            return add(scanner -> scanner.criteriaBuilder().countDistinct(function.apply(clause()).generate(scanner)));
        }


        default T function(String name, Class<?> clazz, Consumer<Registry> argumentSetter){
            return add(ns -> {
                final Registry.Impl impl = new Registry.Impl(new LinkedList<>());
                argumentSetter.accept(impl);
                return ns.criteriaBuilder().function(name, clazz, impl.generate(ns));
            });
        }

        static Factory<SelectClause> clause(){
            return selectClause -> selectClause;
        }
    }

    interface Registry extends Factory<Registry> {
        Registry fields(String... fields);

        record Impl(Collection<SelectClause> store) implements Noodle.Generator<Expression<?>[]>, Registry {


            public Impl(){
                this(new ArrayList<>());
            }

            @Override
            public Registry add(SelectClause selectClause){
                Optional.ofNullable(selectClause).ifPresent(store()::add);
                return this;
            }

            @Override
            public Expression<?>[] generate(NoodleScanner scanner){
                return store().stream().filter(Objects::nonNull).map(i -> i.generate(scanner)).filter(Objects::nonNull).toArray(Expression[]::new);
            }

            public Registry fields(String... fields){
                Stream.of(fields).forEach(this::field);
                return this;
            }

        }
    }


    interface Column extends SelectClause {

        String field();

        default String alias(){
            return null;
        }

        @Override
        default Path<?> generate(NoodleScanner scanner){
            String[] fieldAlias = field().split(" as ", 2);
            return switch(fieldAlias.length){
                case 1 -> {
                    final Path<?> path = scanner.findField(fieldAlias[0]);
                    Optional.ofNullable(alias()).or(() -> Optional.of(fieldAlias[0])).ifPresent(path::alias);
                    yield path;
                }
                case 2 -> {
                    final Path<?> path = scanner.findField(fieldAlias[0]);
                    path.alias(fieldAlias[1]);
                    yield path;
                }
                default -> null;
            };
        }
    }

    interface ChainedSelect extends SelectClause {
        default SelectClause selectClause(){
            return null;
        }

        @Override
        default Expression<?> generate(NoodleScanner scanner){
            if(selectClause() == null){
                return null;
            }
            return selectClause().generate(scanner);
        }
    }

    interface Math extends ChainedSelect {

        default Math plus(Function<Factory<SelectClause>,SelectClause> function){
            return action(function, (scanner, left, right) -> scanner.criteriaBuilder().sum(left, right));
        }

        default Math plus(Number value){
            return plus(s -> s.literal(value));
        }

        default Math minus(Function<Factory<SelectClause>,SelectClause> function){
            return action(function, ((scanner, left, right) -> scanner.criteriaBuilder().diff(left, right)));
        }

        default Math minus(Number value){
            return minus(s -> s.literal(value));
        }

        default Math multiply(Function<Factory<SelectClause>,SelectClause> function){
            return action(function, ((scanner, left, right) -> scanner.criteriaBuilder().prod(left, right)));
        }

        default Math multiply(Number value){
            return multiply(s -> s.literal(value));
        }

        default Math divide(Function<Factory<SelectClause>,SelectClause> function){
            return action(function, ((scanner, left, right) -> scanner.criteriaBuilder().quot(left, right)));
        }

        default Math divide(Number value){
            return divide(s -> s.literal(value));
        }

        default Math action(Function<Factory<SelectClause>,SelectClause> function, ExpressionMerge<Number> merger){
            final SelectClause left = selectClause();
            final SelectClause right = function.apply(Factory.clause());

            if(left != null && right != null){

                return new Math() {
                    @Override
                    public SelectClause selectClause(){
                        return scanner -> {
                            final Expression<?> _left = left.generate(scanner);
                            final Expression<?> _right = right.generate(scanner);
                            if(! Number.class.isAssignableFrom(_left.getJavaType())){
                                throw new IllegalArgumentException(String.format(
                                        "java type of expression %s for math() operation should аppointed from Number.class (current type %s)",
                                        _left,
                                        _left.getJavaType()));
                            }else if(! Number.class.isAssignableFrom(_right.getJavaType())){
                                throw new IllegalArgumentException(String.format(
                                        "java type of expression %s for math() operation should аppointed from Number.class (current type %s)",
                                        _right,
                                        _right.getJavaType()));
                            }
                            return merger.apply(scanner, (Expression<Number>) _left, (Expression<Number>) _right);
                        };
                    }
                };
            }else if(right != null){
                return new Math() {
                    @Override
                    public SelectClause selectClause(){
                        return right;
                    }
                };
            }

            return this;
        }

        @FunctionalInterface
        interface ExpressionMerge<T> {

            Expression<T> apply(NoodleScanner scanner, Expression<T> left, Expression<T> right);
        }
    }

    interface Coalesce extends ChainedSelect {

        default Coalesce or(Object value){
            return or(f -> f.literal(value));
        }

        default Coalesce or(Function<Factory<SelectClause>,SelectClause> function){
            final SelectClause left = selectClause();
            final SelectClause right = function.apply(Factory.clause());

            if(left != null && right != null){

                return new Coalesce() {
                    @Override
                    public SelectClause selectClause(){
                        return scanner -> {
                            final Expression<?> _left = left.generate(scanner);
                            final Expression<?> _right = right.generate(scanner);
                            return scanner.criteriaBuilder().coalesce(_left, _right);
                        };
                    }
                };
            }else if(right != null){
                return new Coalesce() {
                    @Override
                    public SelectClause selectClause(){
                        return right;
                    }
                };
            }

            return this;
        }

    }

    interface Concat extends ChainedSelect {

        default Concat with(String value){
            return with(f -> f.literal(value));
        }

        default Concat with(Function<Factory<SelectClause>,SelectClause> function){
            final SelectClause left = selectClause();
            final SelectClause right = function.apply(Factory.clause());

            if(left != null && right != null){

                return new Concat() {
                    @Override
                    public SelectClause selectClause(){
                        return scanner -> {
                            Expression<?> _left = left.generate(scanner);
                            Expression<?> _right = right.generate(scanner);

                            if(! String.class.isAssignableFrom(_left.getJavaType())){
                                _left = _left.as(String.class);
                            }

                            if(! String.class.isAssignableFrom(_right.getJavaType())){
                                _right = _right.as(String.class);
                            }

                            return scanner.criteriaBuilder().concat((Expression<String>) _left, (Expression<String>) _right);
                        };
                    }
                };
            }else if(right != null){
                return new Concat() {
                    @Override
                    public SelectClause selectClause(){
                        return right;
                    }
                };
            }

            return this;
        }

    }


    interface SubQuery<T, W> {

        static <X, Y> SubQuery<X,Y> of(Class<X> tClass, String alias, Function<QueryClause,Y> function){
            return new SubQuery.Impl<>(tClass, alias, function);
        }

        Class<T> entity();

        <X> W select(Function<Factory<SelectClause>,SelectClause> function, Class<X> as);

        SubQuery<T,W> whereClause(Function<WhereClause.InitChain,WhereClause.Factory> function);

        SubQuery<T,W> group(Consumer<SelectClause.Registry> consumer);

        SubQuery<T,W> having(Function<WhereClause.InitChain,WhereClause.Factory> function);

        record Impl<T, W>(Class<T> entity, String alias, Function<QueryClause,W> applyer, WhereClause.Registry whereRegistry, SelectClause.Registry.Impl groupRegistry,
                          WhereClause.Registry havingRegistry) implements SubQuery<T,W> {

            public Impl(Class<T> entity, String alias, Function<QueryClause,W> applyer){
                this(entity,
                     alias,
                     applyer,
                     new WhereClause.Registry(new ArrayList<>(), Predicate.BooleanOperator.AND, false),
                     new SelectClause.Registry.Impl(),
                     new WhereClause.Registry(new ArrayList<>(), Predicate.BooleanOperator.AND, false));
            }

            @Override
            public <X> W select(Function<Factory<SelectClause>,SelectClause> function, Class<X> as){
                assert as != null;
                assert function != null;
                return applyer().apply(superScanner -> {

                    final Subquery<X> subquery = superScanner.query().subquery(as);
                    Root<T> root = subquery.from(entity);

                    final NoodleScanner scanner = superScanner.build(alias(), root, subquery);

                    final Expression<?> generate = function.apply(sc -> sc).generate(scanner);
                    assert generate != null;
                    if(generate.getJavaType() == null || as.isAssignableFrom(generate.getJavaType())){
                        subquery.select((Expression<X>) generate);
                    }else{
                        subquery.select(generate.as(as));
                    }

                    Optional.ofNullable(whereRegistry.generate(scanner)).ifPresent(subquery::where);
                    Optional.of(groupRegistry.generate(scanner)).filter(i -> i.length > 0).ifPresent(subquery::groupBy);
                    Optional.ofNullable(havingRegistry().generate(scanner)).ifPresent(subquery::having);


                    return subquery;
                });
            }

            @Override
            public SubQuery<T,W> whereClause(Function<WhereClause.InitChain,WhereClause.Factory> function){
                whereRegistry().and().clause(function);
                return this;
            }

            @Override
            public SubQuery<T,W> group(Consumer<Registry> consumer){
                consumer.accept(groupRegistry());
                return this;
            }

            @Override
            public SubQuery<T,W> having(Function<WhereClause.InitChain,WhereClause.Factory> function){
                havingRegistry().and().clause(function);
                return this;
            }

            private record RecordSubQueryGenerator<T>(Subquery<? extends T> subquery, NoodleScanner noodleScanner) {
                static <T, W> RecordSubQueryGenerator<T> from(Class<T> tclass, Class<W> entity, NoodleScanner superScanner, String alias){
                    final Subquery<? extends T> subquery = superScanner.query().subquery(tclass);
                    final Root<W> from = subquery.from(entity);
                    return new RecordSubQueryGenerator<>(subquery, superScanner.build(alias, from, subquery));
                }
            }
        }
    }


}
