package io.gitlab.paa.coder.noodlecriteriaapi.utils;

import io.gitlab.paa.coder.noodlecriteriaapi.Noodle;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Tuple;
import jakarta.persistence.TypedQuery;

import java.util.Optional;
import java.util.stream.Stream;

public interface QueryProcessor<X> {
    default Stream<X> stream() {
        return stream(null);
    }

    default Stream<X> stream(Integer count){
        return stream(count, null);
    }

    Stream<X> stream(Integer count, Integer offset);
    Long total();

    static <T> Mono<T> from(Noodle.SelectQuery<T> selectQuery,EntityManager entityManager){
        return new Mono<>(selectQuery,entityManager);
    }

    static <T> Multi<T> from(Noodle.MultiSelectQuery<T> selectQuery,EntityManager entityManager){
        return new Multi<>(selectQuery,entityManager);
    }

    static <T> Update<T> from(Noodle.UpdateQuery<T> updateQuery,EntityManager entityManager){
        return new Update<>(updateQuery,entityManager);
    }

    static <T> Delete<T> from(Noodle.DeleteQuery<T> deleteQuery,EntityManager entityManager){
        return new Delete<>(deleteQuery,entityManager);
    }


    record Mono<T>(Noodle.SelectQuery<T> selectQuery,EntityManager entityManager) implements  NoodleQuery.Mono<T,Multi<T>,Mono<T>>,QueryProcessor<T>{
        @Override
        public Multi<T> from(Noodle.MultiSelectQuery<T> selectQuery){
            return QueryProcessor.from(selectQuery,entityManager());
        }

        @Override
        public Mono<T> _self(){
            return this;
        }

        @Override
        public Stream<T> stream(Integer count, Integer offset){
            final TypedQuery<T> query = entityManager.createQuery(query());
            Optional.ofNullable(count).ifPresent(i -> {
                query.setMaxResults(i);
                Optional.ofNullable(offset).ifPresent(query::setFirstResult);
            });
            return query.getResultStream();
        }

        @Override
        public Long total(){
            return entityManager.createQuery(count()).getSingleResult();
        }
    }

    record Multi<T>(Noodle.MultiSelectQuery<T> selectQuery,EntityManager entityManager) implements NoodleQuery.Multi<T,Multi<T>>, QueryProcessor<NoodleQuery.Multi.Result>{
        @Override
        public Multi<T> _self(){
            return this;
        }

        @Override
        public Stream<Result> stream(Integer count, Integer offset){
            final TypedQuery<Tuple> query = entityManager.createQuery(query());
            Optional.ofNullable(count).ifPresent(i -> {
                query.setMaxResults(i);
                Optional.ofNullable(offset).ifPresent(query::setFirstResult);
            });
            return query.getResultStream().map(tuple -> {
                NoodleQuery.Multi.Result m = new NoodleQuery.Multi.Result();
                tuple.getElements().forEach(e -> m.put(e.getAlias(), tuple.get(e)));
                return m;
            });
        }

        @Override
        public Long total(){
            return entityManager.createQuery(count()).getSingleResult();
        }
    }

    record Update<T>(Noodle.UpdateQuery<T> updateQuery,EntityManager entityManager) implements NoodleQuery.Update<T,Update<T>>{
        @Override
        public Update<T> _self(){
            return this;
        }

        public int execute(){
            return entityManager.createQuery(update()).executeUpdate();
        }
    }


    record Delete<T>(Noodle.DeleteQuery<T> deleteQuery,EntityManager entityManager) implements NoodleQuery.Delete<T,Delete<T>>{
        @Override
        public Delete<T> _self(){
            return this;
        }

        public int execute(){
            return entityManager.createQuery(delete()).executeUpdate();
        }
    }
}
