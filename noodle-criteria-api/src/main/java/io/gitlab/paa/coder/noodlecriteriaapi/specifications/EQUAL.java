package io.gitlab.paa.coder.noodlecriteriaapi.specifications;

import io.gitlab.paa.coder.noodlecriteriaapi.NoodleScanner;
import io.gitlab.paa.coder.noodlecriteriaapi.WhereClause;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Predicate;

public interface EQUAL extends NoodleSpecification.Interceptor {

    @Override
    default Predicate get(NoodleSpecification spec, NoodleScanner scanner){
        if(spec.value()!=null){
            return new Val(spec, scanner).get();
        }else if(spec.right()!=null){
            return new Exp(spec, scanner).get();
        }
        return null;
    }

    interface Common {

        NoodleSpecification spec();

        Predicate eq();

        default Predicate get(){
            return switch(spec().operator().toUpperCase()){
                case "=" -> eq();
                case "!=", "<>" -> eq().not();
                default -> null;
            };
        }
    }

    record Val(NoodleSpecification spec, NoodleScanner scanner) implements Common {
        @Override
        public Predicate eq(){
            final Object convert;
            if(spec.left().getJavaType() == null || spec.left().getJavaType().isAssignableFrom(spec.value().getClass())){
                convert = spec.value();
            }else{
                try{
                    convert = scanner.specificationConverter().convert(spec.value(), spec.left().getJavaType());
                }catch(Throwable t){
                    throw new WhereClause.SpecificationException(String.format("error convert object %s to %s(%s) for criteria %s",
                                                                               spec.value(),
                                                                               spec.left().getJavaType(),
                                                                               spec.left(),
                                                                               spec.operator()), t);
                }
            }

            return scanner.criteriaBuilder().equal(spec.left(), convert);
        }
    }

    record Exp(NoodleSpecification spec, NoodleScanner scanner) implements Common {
        @Override
        public Predicate eq(){
            final Expression<?> _right;
            if(spec.left().getJavaType() != null && spec.right().getJavaType() != null && spec.left().getJavaType().isAssignableFrom(spec.right().getJavaType())){
                _right = spec.right();
            }else{
                _right = spec.right().as(spec.left().getJavaType());
            }

            return scanner.criteriaBuilder().equal(spec.left(), _right);
        }
    }
}
