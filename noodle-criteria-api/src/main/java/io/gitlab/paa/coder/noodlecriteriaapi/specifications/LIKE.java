package io.gitlab.paa.coder.noodlecriteriaapi.specifications;

import io.gitlab.paa.coder.noodlecriteriaapi.NoodleScanner;
import io.gitlab.paa.coder.noodlecriteriaapi.WhereClause;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Predicate;

public interface LIKE extends NoodleSpecification.Interceptor {

    @Override
    default Predicate get(NoodleSpecification spec, NoodleScanner scanner){
        if(spec.value()!=null){
            return new Val(spec, scanner).get();
        }else if(spec.right()!=null){
            return new Exp(spec, scanner).get();
        }
        return null;
    }

    interface Common {

        NoodleSpecification spec();
        NoodleScanner scanner();

        Predicate like(boolean toLower);

        default Expression<String> tryCast(Expression<?> expression,boolean toLowerCase){
            final Expression<String> _expression;
            if(expression.getJavaType() == null || String.class.isAssignableFrom(expression.getJavaType())){
                _expression=(Expression<String>)expression;
            }else {
                _expression = expression.as(String.class);
            }
            if(toLowerCase){
                return scanner().criteriaBuilder().lower(_expression);
            }
            return _expression;
        }
        default Predicate get(){
            return switch(spec().operator().toUpperCase()){
                case "LIKE" -> like(false);
                case "ILIKE" -> like(true);
                default -> null;
            };
        }
    }

    record Val(NoodleSpecification spec, NoodleScanner scanner) implements Common {
        @Override
        public Predicate like(boolean toLowerCase){
            final String convert;
            if(String.class.isAssignableFrom(spec.value().getClass())){
                convert = (String) spec.value();
            }else{
                try{
                    convert = scanner.specificationConverter().convert(spec.value(), String.class);
                }catch(Throwable t){
                    throw new WhereClause.SpecificationException(String.format("error convert object %s to %s for criteria %s", spec.value(), String.class, spec.operator()), t);
                }
            }

            return scanner.criteriaBuilder().like(tryCast(spec().left(),toLowerCase), toLowerCase?convert.toLowerCase():convert);
        }
    }

    record Exp(NoodleSpecification spec, NoodleScanner scanner) implements Common {
        @Override
        public Predicate like(boolean toLowerCase){
            return scanner.criteriaBuilder().like(tryCast(spec().left(),toLowerCase), tryCast(spec().right(),toLowerCase));
        }
    }
}
