package io.gitlab.paa.coder.noodlecriteriaapi;

import jakarta.persistence.criteria.CriteriaUpdate;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Path;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

public interface SetClause extends Noodle.Worker<CriteriaUpdate<?>> {


    record Registry(Collection<SetClause> store) implements Noodle.Worker<CriteriaUpdate<?>>, Factory {

        public Registry(){
            this(new ArrayList<>());
        }

        @Override
        public Factory add(SetClause selectClause){
            Optional.ofNullable(selectClause).ifPresent(store()::add);
            return this;
        }

        @Override
        public void apply(NoodleScanner scanner, CriteriaUpdate<?> criteriaUpdate){
            store().stream().filter(Objects::nonNull).forEach(i -> i.apply(scanner, criteriaUpdate));
        }

    }

    interface Factory {
        Factory add(SetClause orderClause);

        default InitChain<Factory> set(String field){
            return InitChain.of(field, this::add);
        }
        default Factory set(String field,Object value){
             return set(field).value(value);
        }
    }


    interface InitChain<T> extends SelectClause.Factory<T> {

        static <X> InitChain<X> of(String field, Function<SetClause,X> function){
            return new Impl<>(field, function);
        }

        T value(Object value);

        record Impl<T>(String field, Function<SetClause,T> function) implements InitChain<T> {

            @Override
            public T add(SelectClause selectClause){
                return function.apply(new Specification() {

                    @Override
                    public String field(){
                        return field;
                    }

                    @Override
                    public <T> void setByPath(NoodleScanner scanner, CriteriaUpdate<?> criteriaUpdate, Path<T> path){
                        Optional.ofNullable(selectClause.generate(scanner)).map(r -> {
                            if(r.getJavaType() == null || path.getJavaType().isAssignableFrom(r.getJavaType())){
                                return (Expression<T>) r;
                            }else{
                                return r.as(path.getJavaType());
                            }
                        }).ifPresent(r -> {
                            criteriaUpdate.set(path, r);
                        });
                    }
                });
            }

            public T value(Object value){
                return function.apply(new Specification() {

                    @Override
                    public String field(){
                        return field;
                    }

                    @Override
                    public <T> void setByPath(NoodleScanner scanner, CriteriaUpdate<?> criteriaUpdate, Path<T> path){
                        final T convert;
                        if(value == null || path.getJavaType().isAssignableFrom(value.getClass())){
                            convert = (T) value;
                        }else{
                            try{
                                convert = scanner.specificationConverter().convert(value, path.getJavaType());
                            }catch(Throwable t){
                                throw new WhereClause.SpecificationException(String.format("error convert object %s to %s(%s) for set operation",
                                                                                           value,
                                                                                           path.getJavaType(),
                                                                                           path), t);
                            }
                        }
                        criteriaUpdate.set(path,convert);
                    }
                });
            }
        }
    }

    interface Specification extends SetClause {

        String field();

        <T> void setByPath(NoodleScanner scanner, CriteriaUpdate<?> criteriaUpdate, Path<T> path);

        @Override
        default void apply(NoodleScanner scanner, CriteriaUpdate<?> criteriaUpdate){
            Optional.ofNullable(scanner.findField(field())).ifPresent(l -> setByPath(scanner, criteriaUpdate, l));
        }
    }


}
