package io.gitlab.paa.coder.noodlecriteriaapi;

import jakarta.persistence.criteria.Order;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

public interface OrderClause extends Noodle.Generator<Order> {

    record Registry(Collection<OrderClause> store) implements Noodle.Generator<Order[]>, Factory {

        public Registry(){
            this(new ArrayList<>());
        }

        @Override
        public Factory add(OrderClause selectClause){
            Optional.ofNullable(selectClause).ifPresent(store()::add);
            return this;
        }

        @Override
        public Order[] generate(NoodleScanner scanner){
            return store().stream().filter(Objects::nonNull).map(i -> i.generate(scanner)).filter(Objects::nonNull).toArray(Order[]::new);
        }

    }
    interface Factory {
        Factory add(OrderClause orderClause);

        default InitChain<Factory> asc(){
            return chain(true);
        }

        default InitChain<Factory> desc(){
            return chain(false);
        }

        default InitChain<Factory> chain(boolean isAsc){
            return InitChain.of(isAsc,this::add);
        }
    }



    interface InitChain<T> extends SelectClause.Factory<T> {

        default T fields(String... fields){
            assert fields.length>0;
            return Stream.of(fields).reduce((T)null,(l,r)->field(r),(l,r)->r);
        }
        static <X> InitChain<X> of(boolean asc,Function<OrderClause,X> function){
            return new Impl<>(asc,function);
        }
        record Impl<T>(boolean isAsc, Function<OrderClause,T> function) implements InitChain<T> {

            @Override
            public T add(SelectClause selectClause){
                return function.apply(scanner -> Optional.ofNullable(selectClause).map(i -> i.generate(scanner)).map(expression -> {
                    if(isAsc()){
                        return scanner.criteriaBuilder().asc(expression);
                    }
                    return scanner.criteriaBuilder().desc(expression);
                }).orElse(null));
            }
        }
    }


}
