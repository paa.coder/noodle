package io.gitlab.paa.coder.noodlecriteriaapi.specifications;

import io.gitlab.paa.coder.noodlecriteriaapi.NoodleScanner;
import io.gitlab.paa.coder.noodlecriteriaapi.WhereClause;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Predicate;

import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;

public interface COMPARISON extends NoodleSpecification.Interceptor {

    @Override
    default Predicate get(NoodleSpecification spec, NoodleScanner scanner){
        if(spec.value() != null){
            return new Val(spec, scanner).get();
        }else if(spec.right() != null){
            return new Exp(spec, scanner).get();
        }
        return null;
    }

    interface Common {

        NoodleSpecification spec();

        Predicate lt();

        Predicate gt();

        Predicate le();

        Predicate ge();

        default Predicate get(){
            return switch(spec().operator().toUpperCase()){
                case ">" -> gt();
                case ">=" -> ge();
                case "<" -> lt();
                case "<=" -> le();
                default -> null;
            };
        }
    }

    record Exp(NoodleSpecification spec, NoodleScanner scanner) implements Common {

        private <T> Predicate tryCast(Expression<?> left, Expression<?> right, Class<T> tClass, BiFunction<Expression<T>,Expression<T>,Predicate> function){
            if(left.getJavaType() != null && tClass.isAssignableFrom(left.getJavaType())){
                Expression<T> _left = (Expression<T>) left;
                Expression<T> _right;

                Class<?> compareWith = Objects.equals(tClass,Number.class)?Number.class:_left.getJavaType();

                if(right.getJavaType() != null && compareWith.isAssignableFrom(right.getJavaType())){
                    _right = (Expression<T>) right;
                }else{
                    _right = (Expression<T>) right.as(_left.getJavaType());
                }
                return function.apply(_left, _right);
            }

            return null;

        }

        @Override
        public Predicate lt(){
            return Optional
                    .ofNullable(tryCast(spec().left(), spec().right(), Number.class, (l, r) -> scanner().criteriaBuilder().lt(l, r)))
                    .or(() -> Optional.ofNullable(tryCast(spec().left(), spec().right(), Comparable.class, (l, r) -> scanner().criteriaBuilder().lessThan(l, r))))
                    .orElseThrow(this::error);
        }

        @Override
        public Predicate gt(){
            return Optional
                    .ofNullable(tryCast(spec().left(), spec().right(), Number.class, (l, r) -> scanner().criteriaBuilder().gt(l, r)))
                    .or(() -> Optional.ofNullable(tryCast(spec().left(), spec().right(), Comparable.class, (l, r) -> scanner().criteriaBuilder().greaterThan(l, r))))
                    .orElseThrow(this::error);
        }

        @Override
        public Predicate ge(){
            return Optional
                    .ofNullable(tryCast(spec().left(), spec().right(), Number.class, (l, r) -> scanner().criteriaBuilder().ge(l, r)))
                    .or(() -> Optional.ofNullable(tryCast(spec().left(),
                                                          spec().right(),
                                                          Comparable.class,
                                                          (l, r) -> scanner().criteriaBuilder().greaterThanOrEqualTo(l, r))))
                    .orElseThrow(this::error);
        }

        @Override
        public Predicate le(){
            return Optional
                    .ofNullable(tryCast(spec().left(), spec().right(), Number.class, (l, r) -> scanner().criteriaBuilder().le(l, r)))
                    .or(() -> Optional.ofNullable(tryCast(spec().left(),
                                                          spec().right(),
                                                          Comparable.class,
                                                          (l, r) -> scanner().criteriaBuilder().lessThanOrEqualTo(l, r))))
                    .orElseThrow(this::error);
        }

        private WhereClause.SpecificationException error(){
            return new WhereClause.SpecificationException(String.format("expression %s can not be compared(%s) with expression %s",
                                                                        spec().left(),
                                                                        spec().operator(),
                                                                        spec().right()));
        }
    }

    record Val(NoodleSpecification spec, NoodleScanner scanner) implements Common {

        private <T> Predicate tryCast(Expression<?> left, Object value, Class<T> tClass, BiFunction<Expression<T>,T,Predicate> function){
            if(left.getJavaType() != null && tClass.isAssignableFrom(left.getJavaType())){
                Expression<T> _left = (Expression<T>) left;
                final T convert;
                if(_left.getJavaType().isAssignableFrom(value.getClass())){
                    convert = (T) value;
                }else{
                    try{
                        convert = scanner.specificationConverter().convert(value, _left.getJavaType());
                    }catch(Throwable t){
                        throw new WhereClause.SpecificationException(String.format("error convert object %s to %s(by %s) for criteria %s",
                                                                                   value,
                                                                                   left.getJavaType(),
                                                                                   left,
                                                                                   spec.operator()), t);
                    }
                }

                return function.apply(_left, convert);
            }
            return null;
        }

        private WhereClause.SpecificationException error(){
            return new WhereClause.SpecificationException(String.format("expression %s can not be compared by operator %s with value %s",
                                                                        spec.left(),
                                                                        spec.operator(),
                                                                        spec.value()));
        }

        @Override
        public Predicate lt(){
            return Optional
                    .ofNullable(tryCast(spec().left(), spec().value(), Number.class, (l, r) -> scanner().criteriaBuilder().lt(l, r)))
                    .or(() -> Optional.ofNullable(tryCast(spec().left(), spec().value(), Comparable.class, (l, r) -> scanner().criteriaBuilder().lessThan(l, r))))
                    .orElseThrow(this::error);
        }

        @Override
        public Predicate gt(){
            return Optional
                    .ofNullable(tryCast(spec().left(), spec().value(), Number.class, (l, r) -> scanner().criteriaBuilder().gt(l, r)))
                    .or(() -> Optional.ofNullable(tryCast(spec().left(), spec().value(), Comparable.class, (l, r) -> scanner().criteriaBuilder().greaterThan(l, r))))
                    .orElseThrow(this::error);
        }

        @Override
        public Predicate ge(){
            return Optional
                    .ofNullable(tryCast(spec().left(), spec().value(), Number.class, (l, r) -> scanner().criteriaBuilder().ge(l, r)))
                    .or(() -> Optional.ofNullable(tryCast(spec().left(),
                                                          spec().value(),
                                                          Comparable.class,
                                                          (l, r) -> scanner().criteriaBuilder().greaterThanOrEqualTo(l, r))))
                    .orElseThrow(this::error);
        }

        @Override
        public Predicate le(){
            return Optional
                    .ofNullable(tryCast(spec().left(), spec().value(), Number.class, (l, r) -> scanner().criteriaBuilder().le(l, r)))
                    .or(() -> Optional.ofNullable(tryCast(spec().left(),
                                                          spec().value(),
                                                          Comparable.class,
                                                          (l, r) -> scanner().criteriaBuilder().lessThanOrEqualTo(l, r))))
                    .orElseThrow(this::error);
        }
    }
}
