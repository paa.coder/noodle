package io.gitlab.paa.coder.noodlecriteriaapi;

import io.gitlab.paa.coder.noodlecriteriaapi.specifications.NoodleSpecification;
import jakarta.persistence.criteria.CommonAbstractCriteria;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Path;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public interface NoodleScanner {

    SpecificationConverter specificationConverter();
    CriteriaBuilder criteriaBuilder();

    default NoodleScanner build(String alias, Path<?> root, CommonAbstractCriteria query){
        return new SubScanner(this,alias,root,query);
    }
    Path<?> findField(String field);
    CommonAbstractCriteria query();

    interface Builder{
        SpecificationConverter specificationConverter();
        CriteriaBuilder criteriaBuilder();

        default NoodleScanner build(Path<?> root, CommonAbstractCriteria query){
            return new Impl(specificationConverter(), criteriaBuilder(), root, query);
        }
    }

    interface SpecificationConverter {
        default <X> X convert(Object o, Class<X> clazz){
            return (X) o;
        }

        default List<NoodleSpecification.Interceptor> specificationFilters(){
            return NoodleSpecification.Interceptor.defaultList();
        }
    }


    interface ChainedField extends Iterable<String>{
        String field();

        @Override
        default Iterator<String> iterator(){
            return Arrays.stream(field().split("\\.")).iterator();
        }
    }

    interface PathFinder {
        Path<?> root();

        default Path<?> get(Iterator<String> iterator){
            assert iterator.hasNext();
            final Path<?> next =  root().get(iterator.next());

            if(iterator.hasNext()){
                return ((PathFinder) () -> next).get(iterator);
            }
            return next;
        }
    }

    record Impl(SpecificationConverter specificationConverter, CriteriaBuilder criteriaBuilder, Path<?> root, CommonAbstractCriteria query) implements NoodleScanner {

        @Override
        public Path<?> findField(String field){
            assert field !=null;
            final Iterator<String> iterator = ((ChainedField) () -> field).iterator();
            return ((PathFinder) () -> root).get(iterator);
        }
    }

    record SubScanner(NoodleScanner sub, String alias, Path<?> root, CommonAbstractCriteria query) implements NoodleScanner {
        @Override
        public SpecificationConverter specificationConverter(){
            return sub.specificationConverter();
        }

        @Override
        public CriteriaBuilder criteriaBuilder(){
            return sub().criteriaBuilder();
        }
        @Override
        public Path<?> findField(String field){
            assert field !=null;
            final Iterator<String> iterator = ((ChainedField) () -> field).iterator();
            final String alias = iterator.next();
            if(Objects.equals(alias,alias())){
                if(iterator.hasNext()){
                    return ((PathFinder) () -> root).get(iterator);
                }
                return root();
            }
            return sub().findField(field);
        }




    }


}
