package io.gitlab.paa.coder.noodlecriteriaapi.utils;

import io.gitlab.paa.coder.noodlecriteriaapi.*;
import jakarta.persistence.Tuple;
import jakarta.persistence.criteria.CriteriaDelete;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.CriteriaUpdate;

import java.util.HashMap;
import java.util.function.Consumer;
import java.util.function.Function;

public interface NoodleQuery {

    interface Mono<T,Z extends Multi<T,?>,X extends Mono<T,Z,X>> extends Noodle.SelectQuery<T> {

        Noodle.SelectQuery<T> selectQuery();

        Z from(Noodle.MultiSelectQuery<T> selectQuery);

        X _self();

        @Override
        default WhereClause specification(){
            return selectQuery().specification();
        }

        @Override
        default CriteriaQuery<T> query(){
            return query(null);
        }

        @Override
        default CriteriaQuery<T> query(String alias){
            return selectQuery().query(alias);
        }

        @Override
        default CriteriaQuery<Long> count(){
            return selectQuery().count();
        }

        @Override
        default Class<T> entity(){
            return selectQuery().entity();
        }

        @Override
        default boolean distinct(){
            return selectQuery().distinct();
        }

        @Override
        default X distinct(boolean distinct){
            selectQuery().distinct(distinct);
            return _self();
        }

        @Override
        default X where(Function<WhereClause.InitChain,WhereClause.Factory> function){
            selectQuery().where(function);
            return _self();
        }

        @Override
        default X order(Consumer<OrderClause.Factory> consumer){
            selectQuery().order(consumer);
            return _self();
        }

        @Override
        default Z select(Consumer<SelectClause.Registry> consumer){
            return from(selectQuery().select(consumer));
        }

        @Override
        default Z group(Consumer<SelectClause.Registry> consumer, boolean addToSelect){
            return from(selectQuery().group(consumer, addToSelect));
        }

        @Override
        default Z group(Consumer<SelectClause.Registry> consumer){
            return group(consumer,false);
        }

    }

    interface Multi<T,X extends Multi<T,X>> extends Noodle.MultiSelectQuery<T> {

        Noodle.MultiSelectQuery<T> selectQuery();

        X _self();


        @Override
        default CriteriaQuery<Tuple> query(String alias){
            return selectQuery().query(alias);
        }

        @Override
        default CriteriaQuery<Long> count(){
            return selectQuery().count();
        }

        @Override
        default WhereClause specification(){
            return selectQuery().specification();
        }

        @Override
        default Class<T> entity(){
            return selectQuery().entity();
        }

        @Override
        default boolean distinct(){
            return selectQuery().distinct();
        }

        @Override
        default X distinct(boolean distinct){
            selectQuery().distinct(distinct);
            return _self();
        }

        @Override
        default X where(Function<WhereClause.InitChain,WhereClause.Factory> function){
            selectQuery().where(function);
            return _self();
        }

        @Override
        default X order(Consumer<OrderClause.Factory> consumer){
            selectQuery().order(consumer);
            return _self();
        }

        @Override
        default X select(Consumer<SelectClause.Registry> consumer){
            selectQuery().select(consumer);
            return _self();
        }

        @Override
        default X group(Consumer<SelectClause.Registry> consumer){
            return group(consumer,false);
        }

        @Override
        default X group(Consumer<SelectClause.Registry> consumer, boolean addToSelect){
            selectQuery().group(consumer,addToSelect);
            return _self();
        }

        @Override
        default X having(Function<WhereClause.InitChain,WhereClause.Factory> function){
            selectQuery().having(function);
            return _self();
        }


        class Result extends HashMap<String,Object> {

            public <V> V cast(String key){
                return (V) get(key);
            }
        }
    }

    interface Update<T,X extends Update<T,X>> extends Noodle.UpdateQuery<T>{
        Noodle.UpdateQuery<T> updateQuery();

        X _self();

        @Override
        default Class<T> entity(){
            return updateQuery().entity();
        }

        @Override
        default X where(Function<WhereClause.InitChain,WhereClause.Factory> function){
            updateQuery().where(function);
            return _self();
        }

        @Override
        default X set(Consumer<SetClause.Factory> function){
            updateQuery().set(function);
            return _self();
        }

        @Override
        default CriteriaUpdate<T> update(){
            return updateQuery().update();
        }
    }

    interface Delete<T,X extends Delete<T,X>> extends Noodle.DeleteQuery<T>{
        Noodle.DeleteQuery<T> deleteQuery();

        X _self();

        @Override
        default Class<T> entity(){
            return deleteQuery().entity();
        }

        @Override
        default X where(Function<WhereClause.InitChain,WhereClause.Factory> function){
            deleteQuery().where(function);
            return _self();
        }


        @Override
        default CriteriaDelete<T> delete(){
            return deleteQuery().delete();
        }
    }
}
