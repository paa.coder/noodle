package io.gitlab.paa.coder.samplesync;

import io.gitlab.paa.coder.noodlecriteriaapi.Noodle;
import io.gitlab.paa.coder.noodlecriteriaapi.utils.NoodleQuery;
import io.gitlab.paa.coder.noodlecriteriaapi.utils.QueryProcessor;
import io.gitlab.paa.coder.samplesync.models.UserInfo;
import io.hypersistence.utils.hibernate.query.SQLExtractor;
import jakarta.persistence.Tuple;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.transaction.Transactional;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.query.criteria.HibernateCriteriaBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
class SampleSyncApplicationTests {

    @Autowired
    public SessionFactory entityManagerFactory;

    @Test
    @Transactional
    void contextLoads(){

        final Session entityManager = entityManagerFactory.getCurrentSession();
        final HibernateCriteriaBuilder criteriaBuilder = entityManagerFactory.getCriteriaBuilder();

        final Noodle noodle = () -> criteriaBuilder;

        final CriteriaQuery<Tuple> query = noodle.from(UserInfo.class).select(s->s.field("id")).where(f->f.not().field("id").isNull()).query();

        final Query<Tuple> _query = entityManagerFactory.getCurrentSession().createQuery(query);

        final String from = SQLExtractor.from(_query);


        final List<NoodleQuery.Multi.Result> collect1 = QueryProcessor.from(noodle.from(UserInfo.class),entityManager)
                .select(s -> s.field("id"))
                .where(f -> f.not().field("id").isNull())
                .stream()
                .collect(Collectors.toList());

        System.out.println("ailsk");
    }

}
