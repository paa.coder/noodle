package io.gitlab.paa.coder.samplesync.noodleCriteriaBuilder;

import io.gitlab.paa.coder.noodlecriteriaapi.Noodle;
import io.gitlab.paa.coder.samplesync.models.UserInfo;
import io.gitlab.paa.coder.samplesync.models.company.CompanyStatement;
import io.hypersistence.utils.hibernate.query.SQLExtractor;
import jakarta.persistence.Tuple;
import jakarta.transaction.Transactional;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest
public class OrderClauseTest {

    @Autowired
    public SessionFactory entityManagerFactory;

    public String query(Function<Noodle.SelectQuery<CompanyStatement>,Noodle.QueryFactory<?>> function){
        final Noodle noodle = () -> entityManagerFactory.getCriteriaBuilder();
        final Noodle.SelectQuery<CompanyStatement> from = noodle.from(CompanyStatement.class);
        ;

        final Query<?> query = entityManagerFactory.getCurrentSession().createQuery(function.apply(from).query());

        return SQLExtractor.from(query);
    }



    @Test
    @Transactional
    void order(){
        assertThat(query(s -> s.order(f->f.asc().fields("applicantId","managerId")).distinct(true)))
                .startsWith("select distinct")
                .endsWith("order by c1_0.applicant_id,c1_0.manager_id");

        assertThat(query(s -> s.order(f->f.desc().field("id")).distinct(true)))
                .startsWith("select distinct")
                .endsWith("order by c1_0.id desc");

        assertThat(query(s -> s.order(f->f.asc().fields("applicantId","managerId")).select(r->r.field("id")).distinct(true)))
                .startsWith("select distinct")
                .endsWith("order by c1_0.applicant_id,c1_0.manager_id");

        assertThat(query(s -> s.order(f->f.desc().field("id")).select(r->r.field("id")).distinct(true)))
                .isEqualTo("select distinct c1_0.id from sync.company_statements c1_0 order by 1 desc");

    }

}
