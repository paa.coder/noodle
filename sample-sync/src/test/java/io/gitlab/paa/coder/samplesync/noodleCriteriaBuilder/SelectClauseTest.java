package io.gitlab.paa.coder.samplesync.noodleCriteriaBuilder;

import io.gitlab.paa.coder.noodlecriteriaapi.Noodle;
import io.gitlab.paa.coder.samplesync.models.UserInfo;
import io.gitlab.paa.coder.samplesync.models.company.CompanyStatement;
import io.hypersistence.utils.hibernate.query.SQLExtractor;
import jakarta.persistence.Tuple;
import jakarta.transaction.Transactional;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest
public class SelectClauseTest {

    @Autowired
    public SessionFactory entityManagerFactory;

    public String query(Function<Noodle.SelectQuery<CompanyStatement>,Noodle.MultiSelectQuery<CompanyStatement>> function){
        final Noodle noodle = () -> entityManagerFactory.getCriteriaBuilder();
        final Noodle.SelectQuery<CompanyStatement> from = noodle.from(CompanyStatement.class);
        ;

        final Query<Tuple> query = entityManagerFactory.getCurrentSession().createQuery(function.apply(from).query());

        return SQLExtractor.from(query);
    }

    public String qualityString(String selectClause){
        return String.format("%s from sync.company_statements c1_0",selectClause);
    }

    @Test
    @Transactional
    void math(){
        assertThat(query(s -> s.select(f -> f.math(m->m.plus(_s->_s.literal(5)).plus(_s->_s.literal(5))))))
                .isEqualTo(qualityString("select (5+5)"));

        assertThat(query(s -> s.select(f -> f.math(m->m.plus(_s->_s.field("id")).plus(_s->_s.literal(5))))))
                .isEqualTo(qualityString("select (c1_0.id+5)"));

        assertThat(query(s -> s.select(f -> f.math(m->m.plus(_s->_s.field("id")).plus(_s->_s.math(__s->__s.plus(___s->___s.literal(6)).divide(___s->___s.literal(6))))))))
                .isEqualTo(qualityString("select (c1_0.id+(6/6))"));

    }

    @Test
    @Transactional
    void coalesce(){
        assertThat(query(s -> s.select(f -> f.coalesce(m->m.or(_s->_s.literal(5)).or(_s->_s.literal(6))))))
                .isEqualTo(qualityString("select coalesce(5,6)"));

        assertThat(query(s -> s.select(f -> f.coalesce(m->m.or(_s->_s.field("id")).or(_s->_s.literal(5))))))
                .isEqualTo(qualityString("select coalesce(c1_0.id,5)"));

        assertThat(query(s -> s.select(f -> f.coalesce(m->m.or(_s->_s.field("id")).or(_s->_s.coalesce(__s->__s.or(___s->___s.literal(6)).or(___s->___s.literal(3))))))))
                .isEqualTo(qualityString("select coalesce(c1_0.id,coalesce(6,3))"));

    }

    @Test
    @Transactional
    void concate(){
        assertThat(query(s -> s.select(f -> f.concat(m->m.with(_s->_s.literal(5)).with(_s->_s.literal(6))))))
                .isEqualTo(qualityString("select (cast(5 as text)||cast(6 as text))"));

        assertThat(query(s -> s.select(f -> f.concat(m->m.with(_s->_s.field("id")).with(_s->_s.literal(5))))))
                .isEqualTo(qualityString("select (cast(c1_0.id as text)||cast(5 as text))"));

        assertThat(query(s -> s.select(f -> f.concat(m->m.with(_s->_s.field("id")).with(_s->_s.concat(__s->__s.with(___s->___s.field("properties.ogrn")).with(___s->___s.literal(3))))))))
                .isEqualTo(qualityString("select (cast(c1_0.id as text)||(c1_0.ogrn||cast(3 as text)))"));

    }

    @Test
    @Transactional
    void field(){
        assertThat(query(s -> s.select(f->f.fields("id","properties.ogrn as ogrn"))))
                .isEqualTo(qualityString("select c1_0.id,c1_0.ogrn"));

    }

    @Test
    @Transactional
    void literal(){
        assertThat(query(s -> s.select(f->f.literal("test").literal(Instant.ofEpochSecond(1700124935)).literal(21))))
                .isEqualTo(qualityString("select 'test',timestamp '2023-11-16 12:55:35.000000',21"));

    }

    @Test
    @Transactional
    void nullLiteral(){
        assertThat(query(s -> s.select(f->f.literal(null)._null(BigDecimal.class))))
                .isEqualTo(qualityString("select null,null"));

    }

    @Test
    @Transactional
    void lenght(){
        assertThat(query(s -> s.select(f->f.lenght(_f->_f.field("id")).lenght(_f->_f.field("properties.ogrn")).lenght(_f->_f.literal(5)))))
                .isEqualTo(qualityString("select character_length(cast(c1_0.id as text)),character_length(c1_0.ogrn),character_length(cast(5 as text))"));

    }

    @Test
    @Transactional
    void avg(){
        assertThat(query(s -> s.select(f->f.avg(_f->_f.field("id")).avg(_f->_f.literal(5)))))
                .isEqualTo(qualityString("select avg(c1_0.id),avg(5)"));

        assertThatThrownBy(()->query(s -> s.select(f->f.avg(_f->_f.field("properties.ogrn")).avg(_f->_f.literal(5))))).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    @Transactional
    void sum(){
        assertThat(query(s -> s.select(f->f.sum(_f->_f.field("id")).sum(_f->_f.literal(5)))))
                .isEqualTo(qualityString("select sum(c1_0.id),sum(5)"));

        assertThatThrownBy(()->query(s -> s.select(f->f.sum(_f->_f.field("properties.ogrn")).sum(_f->_f.literal(5))))).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    @Transactional
    void max(){
        assertThat(query(s -> s.select(f->f.max(_f->_f.field("id")).max(_f->_f.field("createdAt")).max(_f->_f.literal(5)))))
                .isEqualTo(qualityString("select max(c1_0.id),max(c1_0.created_at),max(5)"));

        assertThat(query(s -> s.select(f->f.max(_f->_f.field("properties.ogrn")).max(_f->_f.literal(5))))).isEqualTo(qualityString("select max(c1_0.ogrn),max(5)"));

    }

    @Test
    @Transactional
    void min(){
        assertThat(query(s -> s.select(f->f.min(_f->_f.field("id")).min(_f->_f.field("createdAt")).min(_f->_f.literal(5)))))
                .isEqualTo(qualityString("select min(c1_0.id),min(c1_0.created_at),min(5)"));

        assertThat(query(s -> s.select(f->f.min(_f->_f.field("properties.ogrn")).min(_f->_f.literal(5))))).isEqualTo(qualityString("select min(c1_0.ogrn),min(5)"));
    }

    @Test
    @Transactional
    void count(){
        assertThat(query(s -> s.select(f->f.count(_f->_f.field("id")).count(_f->_f.field("createdAt")).count(_f->_f.literal(5)))))
                .isEqualTo(qualityString("select count(c1_0.id),count(c1_0.created_at),count(5)"));
    }

    @Test
    @Transactional
    void subquery(){
        assertThat(query(s -> s.select(f -> f.field("id").query(UserInfo.class,"ui",sub->{
            return sub.whereClause(w->w.field("ui.props.email").is("=").field("properties.ogrn")).select(_f->_f.field("ui.id"), UUID.class);
        })))).isEqualTo("select c1_0.id,(select u1_0.id from sync.users_info u1_0 where u1_0.email=c1_0.ogrn) from sync.company_statements c1_0");


    }

}
