package io.gitlab.paa.coder.samplesync.noodleCriteriaBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.gitlab.paa.coder.noodlecriteriaapi.Noodle;
import io.gitlab.paa.coder.noodlecriteriaapi.NoodleScanner;
import io.gitlab.paa.coder.samplesync.models.company.CompanyStatement;
import io.hypersistence.utils.hibernate.query.SQLExtractor;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.transaction.Transactional;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest
public class CountTest {

    @Autowired
    public SessionFactory entityManagerFactory;


    public Query<Long> query(Function<Noodle.SelectQuery<CompanyStatement>,Noodle.QueryFactory<?>> consumer){
        final Noodle noodle = new Noodle() {

            @Override
            public CriteriaBuilder criteriaBuilder(){
                return entityManagerFactory.getCriteriaBuilder();
            }

            @Override
            public NoodleScanner.SpecificationConverter specificationConverter(){
                return new NoodleScanner.SpecificationConverter() {
                    @Override
                    public <X> X convert(Object o, Class<X> clazz){
                        final ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
                        objectMapper.setDateFormat(new SimpleDateFormat());
                        return objectMapper.convertValue(o, clazz);
                    }
                };
            }
        };


        final Noodle.QueryFactory<?> from = consumer.apply(noodle.from(CompanyStatement.class));

        return entityManagerFactory.getCurrentSession().createQuery(from.count());
    }

    public String queryStr(Function<Noodle.SelectQuery<CompanyStatement>,Noodle.QueryFactory<?>> consumer){


        return SQLExtractor.from(query(consumer));
    }




    public String qualityString(String whereClause){
        return String.format("select c1_0.id from sync.company_statements c1_0 %s", whereClause);
    }

    @Test
    @Transactional
    void count(){
        assertThat(queryStr(s -> s.where(f -> f.field("id").isNull())))
                .isEqualTo("select count(c1_0.id) from sync.company_statements c1_0 where c1_0.id is null");

        assertThat(queryStr(s -> s.where(f -> f.field("id").isNull()).group(f->f.field("properties.name"))))
                .isEqualTo("select count(c1_0.id) from sync.company_statements c1_0 where c1_0.id is null group by c1_0.name");
    }
}
