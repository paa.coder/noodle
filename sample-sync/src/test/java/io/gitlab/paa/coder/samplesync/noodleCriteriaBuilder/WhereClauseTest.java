package io.gitlab.paa.coder.samplesync.noodleCriteriaBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.gitlab.paa.coder.noodlecriteriaapi.Noodle;
import io.gitlab.paa.coder.noodlecriteriaapi.NoodleScanner;
import io.gitlab.paa.coder.noodlecriteriaapi.WhereClause;
import io.gitlab.paa.coder.samplesync.models.company.Company;
import io.gitlab.paa.coder.samplesync.models.company.CompanyStatement;
import io.hypersistence.utils.hibernate.query.SQLExtractor;
import jakarta.persistence.Tuple;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.transaction.Transactional;
import org.assertj.core.api.SoftAssertions;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.query.sqm.tree.expression.ValueBindJpaCriteriaParameter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.util.Pair;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest
public class WhereClauseTest {

    @Autowired
    public SessionFactory entityManagerFactory;


    public Query<Tuple> query(Consumer<Noodle.MultiSelectQuery<CompanyStatement>> consumer){
        final Noodle noodle = new Noodle() {

            @Override
            public CriteriaBuilder criteriaBuilder(){
                return entityManagerFactory.getCriteriaBuilder();
            }

            @Override
            public NoodleScanner.SpecificationConverter specificationConverter(){
                return new NoodleScanner.SpecificationConverter() {
                    @Override
                    public <X> X convert(Object o, Class<X> clazz){
                        final ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
                        objectMapper.setDateFormat(new SimpleDateFormat());
                        return objectMapper.convertValue(o, clazz);
                    }
                };
            }
        };


        final Noodle.MultiSelectQuery<CompanyStatement> from = noodle.from(CompanyStatement.class).select(s -> s.field("id"));
        consumer.accept(from);

        return entityManagerFactory.getCurrentSession().createQuery(from.query());
    }

    public String queryStr(Consumer<Noodle.MultiSelectQuery<CompanyStatement>> consumer){


        return SQLExtractor.from(query(consumer));
    }


    record Param(Object value, Class javaType) {}

    public Pair<String,Param> singleParam(Consumer<Noodle.MultiSelectQuery<CompanyStatement>> consumer){

        final Query<Tuple> query = query(consumer);
        final Param param = query.getParameters().stream().findFirst().map(i -> {
            ValueBindJpaCriteriaParameter _i = (ValueBindJpaCriteriaParameter) i;
            return new Param(_i.getValue(), _i.getJavaType());
        }).orElse(new Param(null, null));
        return Pair.of(SQLExtractor.from(query), param);


    }

    public String qualityString(String whereClause){
        return String.format("select c1_0.id from sync.company_statements c1_0 %s", whereClause);
    }

    @Test
    @Transactional
    void nullCause(){
        assertThat(queryStr(s -> s.where(f -> f.field("id").isNull())))
                .isEqualTo(queryStr(s -> s.where(f -> f.not().field("id").isNotNull())))
                .isEqualTo(qualityString("where c1_0.id is null"));

        assertThat(queryStr(s -> s.where(f -> f.not().field("id").isNull())))
                .isEqualTo(queryStr(s -> s.where(f -> f.field("id").isNotNull())))
                .isEqualTo(qualityString("where c1_0.id is not null"));

        assertThat(queryStr(s -> s.where(f -> f.math(m -> m.plus(_s -> _s.literal(5)).plus(_s -> _s.literal(5))).isNull())))
                .isEqualTo(qualityString("where (5+5) is null"));

        assertThat(queryStr(s -> s.where(f -> f.math(m -> m.plus(_s -> _s.literal(5)).plus(_s -> _s.literal(5))).is(">").literal(10))))
                .isEqualTo(qualityString("where (5+5)>10"));
    }

    @Test
    @Transactional
    void inSpecification(){

        assertThat(queryStr(s -> s.where(f -> f.field("properties.ogrn").is("in").literal(null))))
                .isEqualTo(qualityString("where c1_0.ogrn in (cast(null as text))"));

        assertThat(queryStr(s -> s.where(f -> f.field("properties.name").in().query(Company.class, "c", sub -> {
            return sub.whereClause(w -> w.field("c.properties.name").isNotNull()).select(_s -> _s.field("c.properties.name"), String.class);
        })))).isEqualTo(qualityString(
                "where c1_0.name in ((select c2_0.name from sync.companies c2_0 where c2_0.name is not null))"));
    }

    @Test
    @Transactional
    void inValueSpecification(){

        assertThat(queryStr(s -> s.where(f -> f.field("id").is("in").value(List.of("1", "2")))))
                .isEqualTo(qualityString("where c1_0.id in (?,?)")).as("sample in");

        {
            final ArrayList<Object> objects = new ArrayList<>();
            objects.add(null);
            objects.add(1);

            assertThat(queryStr(s -> s.where(f -> f.field("id").is("in").value(objects))))
                    .isEqualTo(qualityString("where c1_0.id in (?)")).as("in - collection with null values");
        }

        {
            final ArrayList<Object> objects = new ArrayList<>();
            objects.add(null);
            objects.add(null);

            assertThat(queryStr(s -> s.where(f -> f.field("id").is("in").value(objects))))
                    .isEqualTo(queryStr(s -> s.where(f -> f.field("id").is("in").value(List.of()))))
                    .isEqualTo(qualityString("where 1=0")).as("in - empty collection");
        }

        assertThat(queryStr(s -> s.where(f -> f.field("id").is("in").value(null))))
                .isEqualTo(qualityString("where c1_0.id in (cast(null as bigint))")).as("in - null value");

        assertThat(queryStr(s -> s.where(f -> f.field("id").is("in").value(1))))
                .isEqualTo(qualityString("where c1_0.id in (?)")).as("in - not collection value");

        assertThatThrownBy(() -> queryStr(s -> s.where(f -> f.field("id").is("in").value("cqaskjn,"))))
                .isInstanceOf(WhereClause.SpecificationException.class);

        {
            final ArrayList<Object> objects = new ArrayList<>();
            objects.add(null);
            objects.add(1);
            objects.add(";cjkadn");

            assertThatThrownBy(() -> queryStr(s -> s.where(f -> f.field("id").is("in").value(objects))))
                    .isInstanceOf(WhereClause.SpecificationException.class);
        }
    }

    @Test
    @Transactional
    void compareSpecification(){

        assertThat(queryStr(s -> s.where(f -> f.field("id").is("<").literal(100)))).isEqualTo(qualityString("where c1_0.id<100")).as("sample <");
        assertThat(queryStr(s -> s.where(f -> f.field("id").is("<=").literal(100L)))).isEqualTo(qualityString("where c1_0.id<=100")).as("sample <=");
        assertThat(queryStr(s -> s.where(f -> f.field("id").is(">").literal(100D)))).isEqualTo(qualityString("where c1_0.id>100.0")).as("sample >");
        assertThat(queryStr(s -> s.where(f -> f.field("id").is(">=").literal(BigDecimal.valueOf(100L)))))
                .isEqualTo(qualityString("where c1_0.id>=100"))
                .as("sample >=");

        assertThat(queryStr(s -> s.where(f -> f.field("id").is(">").literal("100"))))
                .isEqualTo(qualityString("where c1_0.id>cast('100' as bigint)"))
                .as("literal converting");

        assertThat(queryStr(s -> s.where(f -> f.field("createdAt").is("<").literal(Instant.ofEpochSecond(1700124935)))))
                .isEqualTo(qualityString("where c1_0.created_at<timestamp '2023-11-16 12:55:35.000000'"))
                .as("literal converting long to instant");


        assertThat(queryStr(s -> s.where(f -> f.field("createdAt").is("<").literal("2023-11-16 12:55:35.000000"))))
                .isEqualTo(qualityString("where c1_0.created_at<cast('2023-11-16 12:55:35.000000' as timestamp(6) with time zone)"))
                .as("literal converting string to instant");

        assertThat(queryStr(s -> s.where(f -> f.field("id").is("<").literal("2023-11-16 12:55:35.000000"))))
                .isEqualTo(qualityString("where c1_0.id<cast('2023-11-16 12:55:35.000000' as bigint)"))
                .as("literal converting string to bigint");

        assertThat(queryStr(s -> s.where(f -> f.field("id").is("<").query(Company.class, "c", sub -> {
            return sub.whereClause(w -> w.field("c.properties.name").eq().literal(1)).select(_s -> _s.max(_f -> _f.field("c.statementId")), Integer.class);
        })))).isEqualTo(qualityString("where c1_0.id<(select cast(max(c2_0.statement_id) as integer) from sync.companies c2_0 where c2_0.name=cast(1 as text))"));
    }

    @Test
    @Transactional
    void compareValueSpecification(){

        {

            SoftAssertions phoneBundle = new SoftAssertions();
            final Pair<String,Param> p = singleParam(s -> s.where(f -> f.field("id").is("<").value(100)));
            phoneBundle.assertThat(p.getFirst()).isEqualTo(qualityString("where c1_0.id<?"));
            phoneBundle.assertThat(p.getSecond().value()).isEqualTo(100L);
            phoneBundle.assertThat(p.getSecond().javaType()).isEqualTo(Long.class);
            phoneBundle.assertAll();

        }

        {

            SoftAssertions phoneBundle = new SoftAssertions();
            final Pair<String,Param> p = singleParam(s -> s.where(f -> f.field("id").is("<=").value(100)));
            phoneBundle.assertThat(p.getFirst()).isEqualTo(qualityString("where c1_0.id<=?"));
            phoneBundle.assertThat(p.getSecond().value()).isEqualTo(100L);
            phoneBundle.assertThat(p.getSecond().javaType()).isEqualTo(Long.class);
            phoneBundle.assertAll();

        }

        {

            SoftAssertions phoneBundle = new SoftAssertions();
            final Pair<String,Param> p = singleParam(s -> s.where(f -> f.field("id").is(">").value(100)));
            phoneBundle.assertThat(p.getFirst()).isEqualTo(qualityString("where c1_0.id>?"));
            phoneBundle.assertThat(p.getSecond().value()).isEqualTo(100L);
            phoneBundle.assertThat(p.getSecond().javaType()).isEqualTo(Long.class);
            phoneBundle.assertAll();

        }

        {

            SoftAssertions phoneBundle = new SoftAssertions();
            final Pair<String,Param> p = singleParam(s -> s.where(f -> f.field("id").is(">=").value(100)));
            phoneBundle.assertThat(p.getFirst()).isEqualTo(qualityString("where c1_0.id>=?"));
            phoneBundle.assertThat(p.getSecond().value()).isEqualTo(100L);
            phoneBundle.assertThat(p.getSecond().javaType()).isEqualTo(Long.class);
            phoneBundle.assertAll();

        }

        {

            SoftAssertions phoneBundle = new SoftAssertions();
            final Pair<String,Param> p = singleParam(s -> s.where(f -> f.field("id").is(">").value("100")));
            phoneBundle.assertThat(p.getFirst()).isEqualTo(qualityString("where c1_0.id>?"));
            phoneBundle.assertThat(p.getSecond().value()).isEqualTo(100L);
            phoneBundle.assertThat(p.getSecond().javaType()).isEqualTo(Long.class);
            phoneBundle.assertAll();

        }

        {

            SoftAssertions phoneBundle = new SoftAssertions();
            final Pair<String,Param> p = singleParam(s -> s.where(f -> f.field("createdAt").is("<").value(Instant.ofEpochSecond(1700124935))));
            final Pair<String,Param> p2 = singleParam(s -> s.where(f -> f.field("createdAt").is("<").value("2023-11-16T08:55:35.000000Z")));
            assertThat(p).usingRecursiveComparison().isEqualTo(p2);
            phoneBundle.assertThat(p.getFirst()).isEqualTo(qualityString("where c1_0.created_at<?"));
            phoneBundle.assertThat(p.getSecond().value()).isEqualTo(Instant.ofEpochSecond(1700124935));
            phoneBundle.assertThat(p.getSecond().javaType()).isEqualTo(Instant.class);
            phoneBundle.assertAll();

        }


        assertThatThrownBy(() -> queryStr(s -> s.where(f -> f.field("id").is("<").value("2023-11-16 12:55:35.000000"))))
                .isInstanceOf(WhereClause.SpecificationException.class)
                .as("error convert");


    }

    @Test
    @Transactional
    void likeSpecification(){

        assertThat(queryStr(s -> s.where(f -> f.field("properties.ogrn").is("like").literal("%test%"))))
                .isEqualTo(qualityString("where c1_0.ogrn like '%test%' escape ''"))
                .as("sample like");

        assertThat(queryStr(s -> s.where(f -> f.field("properties.ogrn").is("ilike").literal("%test%"))))
                .isEqualTo(qualityString("where lower(c1_0.ogrn) like lower('%test%') escape ''"))
                .as("sample ilike");

        assertThat(queryStr(s -> s.where(f -> f.field("id").is("like").literal(35))))
                .isEqualTo(qualityString("where cast(c1_0.id as text) like cast(35 as text) escape ''"))
                .as("literal convert like");

        assertThat(queryStr(s -> s.where(f -> f.field("id").is("like").literal(Instant.ofEpochSecond(1700124935)))))
                .isEqualTo(qualityString("where cast(c1_0.id as text) like cast(timestamp '2023-11-16 12:55:35.000000' as text) escape ''"))
                .as("literal convert like");

    }

    @Test
    @Transactional
    void likeValueSpecification(){

        {

            SoftAssertions phoneBundle = new SoftAssertions();
            final Pair<String,Param> p = singleParam(s -> s.where(f -> f.field("properties.ogrn").like("%tEst%")));
            phoneBundle.assertThat(p.getFirst()).isEqualTo(qualityString("where c1_0.ogrn like ? escape ''"));
            phoneBundle.assertThat(p.getSecond().value()).isEqualTo("%tEst%");
            phoneBundle.assertThat(p.getSecond().javaType()).isEqualTo(String.class);
            phoneBundle.assertAll();

        }

        {

            SoftAssertions phoneBundle = new SoftAssertions();
            final Pair<String,Param> p = singleParam(s -> s.where(f -> f.field("properties.ogrn").ilike("%tEst%")));
            phoneBundle.assertThat(p.getFirst()).isEqualTo(qualityString("where lower(c1_0.ogrn) like ? escape ''"));
            phoneBundle.assertThat(p.getSecond().value()).isEqualTo("%test%");
            phoneBundle.assertThat(p.getSecond().javaType()).isEqualTo(String.class);
            phoneBundle.assertAll();

        }

        {

            SoftAssertions phoneBundle = new SoftAssertions();
            final Pair<String,Param> p = singleParam(s -> s.where(f -> f.field("id").is("like").value(35)));
            phoneBundle.assertThat(p.getFirst()).isEqualTo(qualityString("where cast(c1_0.id as text) like ? escape ''"));
            phoneBundle.assertThat(p.getSecond().value()).isEqualTo("35");
            phoneBundle.assertThat(p.getSecond().javaType()).isEqualTo(String.class);
            phoneBundle.assertAll();

        }


        {

            SoftAssertions phoneBundle = new SoftAssertions();
            final Pair<String,Param> p = singleParam(s -> s.where(f -> f.field("properties.ogrn").is("like").value(35)));
            phoneBundle.assertThat(p.getFirst()).isEqualTo(qualityString("where c1_0.ogrn like ? escape ''"));
            phoneBundle.assertThat(p.getSecond().value()).isEqualTo("35");
            phoneBundle.assertThat(p.getSecond().javaType()).isEqualTo(String.class);
            phoneBundle.assertAll();

        }

        {

            SoftAssertions phoneBundle = new SoftAssertions();
            final Pair<String,Param> p = singleParam(s -> s.where(f -> f.field("properties.ogrn").is("ilike").value(Instant.ofEpochSecond(1700124935))));
            phoneBundle.assertThat(p.getFirst()).isEqualTo(qualityString("where lower(c1_0.ogrn) like ? escape ''"));
            phoneBundle.assertThat(p.getSecond().value()).isEqualTo("2023-11-16t08:55:35z");
            phoneBundle.assertThat(p.getSecond().javaType()).isEqualTo(String.class);
            phoneBundle.assertAll();

        }
    }

    @Test
    @Transactional
    void equalsSpecification(){


        assertThat(queryStr(s -> s.where(f -> f.field("properties.ogrn").is("=").literal("test"))))
                .isEqualTo(qualityString("where c1_0.ogrn='test'"))
                .as("sample =");

        assertThat(queryStr(s -> s.where(f -> f.field("properties.ogrn").is("<>").literal("test"))))
                .isEqualTo(queryStr(s -> s.where(f -> f.field("properties.ogrn").is("!=").literal("test"))))
                .isEqualTo(qualityString("where c1_0.ogrn<>'test'"))
                .as("sample <>");

        assertThat(queryStr(s -> s.where(f -> f.field("properties.ogrn").is("<>").literal(1))))
                .isEqualTo(qualityString("where c1_0.ogrn<>cast(1 as text)"))
                .as("cast literal");

        assertThat(queryStr(s -> s.where(f -> f.field("properties.ogrn").is("<>").literal(Instant.ofEpochSecond(1700124935)))))
                .isEqualTo(qualityString("where c1_0.ogrn<>cast(timestamp '2023-11-16 12:55:35.000000' as text)"))
                .as("cast literal");

        assertThat(queryStr(s -> s.where(f -> f.field("id").eq().query(Company.class, "c", sub -> {
            return sub.whereClause(w -> w.field("c.properties.name").eq().literal(1)).select(_s -> _s.max(_f -> _f.field("c.statementId")), Integer.class);
        }))))
                .isEqualTo(qualityString(
                        "where c1_0.id=cast((select cast(max(c2_0.statement_id) as integer) from sync.companies c2_0 where c2_0.name=cast(1 as text)) as bigint)"))
                .as("literal converting string to bigint");

    }


    @Test
    @Transactional
    void equalsValueSpecification(){

        {

            SoftAssertions phoneBundle = new SoftAssertions();
            final Pair<String,Param> p = singleParam(s -> s.where(f -> f.field("properties.ogrn").is("=").value("test")));
            phoneBundle.assertThat(p.getFirst()).isEqualTo(qualityString("where c1_0.ogrn=?"));
            phoneBundle.assertThat(p.getSecond().value()).isEqualTo("test");
            phoneBundle.assertThat(p.getSecond().javaType()).isEqualTo(String.class);
            phoneBundle.assertAll();

        }

        {

            SoftAssertions phoneBundle = new SoftAssertions();
            final Pair<String,Param> p = singleParam(s -> s.where(f -> f.field("properties.ogrn").is("<>").value("test")));
            phoneBundle.assertThat(p.getFirst()).isEqualTo(qualityString("where c1_0.ogrn<>?"));
            phoneBundle.assertThat(p.getSecond().value()).isEqualTo("test");
            phoneBundle.assertThat(p.getSecond().javaType()).isEqualTo(String.class);
            phoneBundle.assertAll();

        }

        {

            SoftAssertions phoneBundle = new SoftAssertions();
            final Pair<String,Param> p = singleParam(s -> s.where(f -> f.field("properties.ogrn").is("<>").value(1)));
            phoneBundle.assertThat(p.getFirst()).isEqualTo(qualityString("where c1_0.ogrn<>?"));
            phoneBundle.assertThat(p.getSecond().value()).isEqualTo("1");
            phoneBundle.assertThat(p.getSecond().javaType()).isEqualTo(String.class);
            phoneBundle.assertAll();

        }

        {

            SoftAssertions phoneBundle = new SoftAssertions();
            final Pair<String,Param> p = singleParam(s -> s.where(f -> f.field("properties.ogrn").is("<>").value(Instant.ofEpochSecond(1700124935))));
            phoneBundle.assertThat(p.getFirst()).isEqualTo(qualityString("where c1_0.ogrn<>?"));
            phoneBundle.assertThat(p.getSecond().value()).isEqualTo("2023-11-16T08:55:35Z");
            phoneBundle.assertThat(p.getSecond().javaType()).isEqualTo(String.class);
            phoneBundle.assertAll();

        }

    }


    @Test
    @Transactional
    void clause(){


        assertThat(queryStr(s -> {
            s.where(f -> {
                return f.field("properties.ogrn").eq("test")
                        .or().field("id").ne(1).and().field("id").le(2);
            });
        })).isEqualTo(qualityString("where (c1_0.ogrn=? or c1_0.id<>?) and c1_0.id<=?"));

        assertThat(queryStr(s -> {
            s.where(f -> {
                return f.field("properties.ogrn").eq("test")
                        .or().clause(_f -> _f.field("id").ne(1).and().field("id").le(2));
            });
        })).isEqualTo(qualityString("where (c1_0.ogrn=? or c1_0.id<>? and c1_0.id<=?)"));

        assertThat(queryStr(s -> {
            s.where(f -> {
                return f.field("properties.ogrn").eq("test")
                        .or().clause(_f -> _f.field("id").ne(1).and().field("id").le(2));
            });
            s.where(f -> {
                return f.field("properties.ogrn").eq("test")
                        .or().clause(_f -> _f.field("id").ne(1).and().field("id").le(2));
            });
        })).isEqualTo(qualityString("where (c1_0.ogrn=? or c1_0.id<>? and c1_0.id<=?) and (c1_0.ogrn=? or c1_0.id<>? and c1_0.id<=?)"));


        assertThat(queryStr(s -> {
            s.where(f -> {
                return f.field("properties.ogrn").eq("test")
                        .and().field("id").ne(1).and().clause(_f -> {
                            return _f.field("properties.ogrn").ne("test")
                                    .or().field("id").eq(1);
                        }).and().clause(_f -> {
                            return _f.clause(__f -> {
                                return __f.not().field("properties.ogrn").eq("tset")
                                        .and().field("id").lt(1);
                            }).or().not().field("id").gt(7);
                        });
            });
        })).isEqualTo(qualityString("where c1_0.ogrn=? and c1_0.id<>? and (c1_0.ogrn<>? or c1_0.id=?) and (c1_0.ogrn<>? and c1_0.id<? or c1_0.id<=?)"));

        assertThat(queryStr(s -> {
            s.where(f -> {
                return f.field("properties.ogrn").eq("test")
                        .and().field("id").ne(1).and().clause(_f -> {
                            return _f.field("properties.ogrn").ne("test")
                                    .or().field("id").eq(1);
                        }).and().clause(_f -> {
                            return _f.field("id").gt(7).and().clause(__f -> {
                                return __f.not().field("properties.ogrn").eq("tset")
                                        .or().field("id").lt(1);
                            });
                        });
            });
        })).isEqualTo(qualityString("where c1_0.ogrn=? and c1_0.id<>? and (c1_0.ogrn<>? or c1_0.id=?) and c1_0.id>? and (c1_0.ogrn<>? or c1_0.id<?)"));


    }

    @Test
    @Transactional
    void existSpecification(){

        assertThat(queryStr(s -> s.where(f -> f.exists(Company.class, "c", sub -> {
            return sub.whereClause(w -> w.field("c.properties.name").eq().literal(1)).select(_s -> _s.max(_f -> _f.field("c.statementId")), Integer.class);
        }))))
                .isEqualTo(qualityString(
                        "where exists(select cast(max(c2_0.statement_id) as integer) from sync.companies c2_0 where c2_0.name=cast(1 as text))"))
                .as("literal converting string to bigint");

    }
}
