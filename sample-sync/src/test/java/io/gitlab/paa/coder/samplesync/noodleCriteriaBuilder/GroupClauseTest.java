package io.gitlab.paa.coder.samplesync.noodleCriteriaBuilder;

import io.gitlab.paa.coder.noodlecriteriaapi.Noodle;
import io.gitlab.paa.coder.samplesync.models.company.CompanyStatement;
import io.hypersistence.utils.hibernate.query.SQLExtractor;
import jakarta.transaction.Transactional;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class GroupClauseTest {

    @Autowired
    public SessionFactory entityManagerFactory;

    public String query(Function<Noodle.SelectQuery<CompanyStatement>,Noodle.QueryFactory<?>> function){
        final Noodle noodle = () -> entityManagerFactory.getCriteriaBuilder();
        final Noodle.SelectQuery<CompanyStatement> from = noodle.from(CompanyStatement.class);
        ;

        final Query<?> query = entityManagerFactory.getCurrentSession().createQuery(function.apply(from).query());

        return SQLExtractor.from(query);
    }


    @Test
    @Transactional
    void group(){
        assertThat(query(s -> {
            return s.where(f -> {
                return f.field("properties.ogrn").eq("test")
                        .or().field("id").ne(1).and().field("id").le(2);
            }).group(f -> f.fields("properties.inn", "properties.name"), true).having(w -> {
                return w.clause(_w -> _w.field("properties.inn").eq(1).or().field("properties.inn").in("10", "11"))
                        .and()
                        .clause(_w -> _w.field("properties.name").eq("10").or().field("properties.name").like("100"));
            }).select(_s -> _s.max(f -> f.field("createdAt")).math(i -> i.plus(f -> f.field("id")).plus(1).divide(2)));
        })).isEqualTo(
                "select c1_0.inn,c1_0.name,max(c1_0.created_at),((c1_0.id+1)/2) from sync.company_statements c1_0 where (c1_0.ogrn=? or c1_0.id<>?) and c1_0.id<=? " +
                        "group by 1,2 having (c1_0.inn=? or c1_0.inn in (?,?)) and (c1_0.name=? or c1_0.name like ? escape '')");

    }

}
