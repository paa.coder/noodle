package io.gitlab.paa.coder.samplesync.config;

import com.zaxxer.hikari.HikariDataSource;
import lombok.Data;
import org.flywaydb.core.Flyway;
import org.hibernate.SessionFactory;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.orm.hibernate5.SessionFactoryUtils;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.AbstractPlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


@Configuration
public class DatabaseConfig {

    @Bean
    @ConfigurationProperties("database-properties")
    public DbProperties dbProperties(){
        return new DbProperties();
    }


    @Bean(name = "flyway")
    public Flyway flyway(DataSource ds){
        Flyway fw = Flyway
                .configure()
                .schemas(dbProperties().getSchema())
                .dataSource(ds)
                .locations("classpath:db/migration")
                .load();
        fw.migrate();
        return fw;
    }


    @Bean(name = "dataSource")
    public HikariDataSource dataSource(){
        HikariDataSource ds = new HikariDataSource();
        ds.setJdbcUrl(dbProperties().getUrl());
        ds.addDataSourceProperty("stringtype", "unspecified");
        ds.setUsername(dbProperties().getUser());
        ds.setPassword(dbProperties().getPassword());
        ds.setMaximumPoolSize(dbProperties().getMaxPoolSize());
        ds.setConnectionTimeout(dbProperties().getConnectionTimeout());
        ds.setConnectionTestQuery("SELECT 1");
        ds.setSchema(dbProperties().getSchema());
        return ds;

    }

    @Primary
    @Bean
    @DependsOn({"flyway"})
    public SessionFactory entityManagerFactory(DataSource dataSource){
        final Map<String,Object> properties = new HashMap<>();
        Optional.ofNullable(dbProperties().getShowSql()).ifPresent(i -> {
            properties.put("hibernate.show_sql", i);
            properties.put("hibernate.format_sql", i);
            properties.put("hibernate.highlight_sql", i);
        });
        Optional.ofNullable(dbProperties().getSchema()).ifPresent(i -> properties.put("hibernate.default_schema", i));
        Optional.ofNullable(dbProperties().getBatchSize()).ifPresent(i -> properties.put("hibernate.jdbc.batch_size", i));
        properties.put("hibernate.connection.datasource", dataSource);

        return new HibernatePersistenceProvider().createEntityManagerFactory("sync", properties).unwrap(SessionFactory.class);
    }

    @Bean
    public PlatformTransactionManager countingTransactionManager(SessionFactory entityManagerFactory, DataSource dataSource){
        JpaTransactionManager tm = new JpaTransactionManager();
        tm.setEntityManagerFactory(entityManagerFactory);
        tm.setTransactionSynchronization(AbstractPlatformTransactionManager.SYNCHRONIZATION_ALWAYS);
        tm.setDataSource(SessionFactoryUtils.getDataSource(entityManagerFactory));
        return tm;
    }

    @Data
    public static class DbProperties {
        private Boolean showSql;
        private String dialect;
        private Integer batchSize;
        private Integer maxPoolSize;
        private String schema;
        private String url;
        private String user;
        private String password;
        private Integer connectionTimeout;

        public String getPassword(){
            return password;
        }
    }


}
