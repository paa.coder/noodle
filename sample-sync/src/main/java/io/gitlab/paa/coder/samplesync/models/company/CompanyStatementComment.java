package io.gitlab.paa.coder.samplesync.models.company;

import io.gitlab.paa.coder.samplesync.models.Manager;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Data
@Entity
@Table(name = "company_statement_comments")
@EqualsAndHashCode(exclude = {"manager","companyStatement"})
@ToString(exclude = {"manager","companyStatement"})
public class CompanyStatementComment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "company_statement_comments_id_seq")
    @SequenceGenerator(name = "company_statement_comments_id_seq", sequenceName = "company_statement_comments_id_seq", allocationSize = 10)
    private Long id;

    @Column(name ="manager_id", updatable = false,insertable = false)
    private UUID managerId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "manager_id", referencedColumnName = "id", updatable = false)
    @NotNull
    private Manager manager;

    @Column(name ="company_statement_id", updatable = false,insertable = false)
    private Long companyStatementId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "company_statement_id", referencedColumnName = "id", updatable = false)
    @NotNull
    private CompanyStatement companyStatement;

    @NotBlank
    private String message;

    @CreationTimestamp
    @Column(name = "created_at")
    private Instant createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    private Instant updatedAt;

    public void setManager(Manager manager){
        Optional.ofNullable(manager).ifPresentOrElse(ui-> this.setManagerId(ui.getId()), ()->this.setManagerId(null));
        this.manager = manager;
    }
}
