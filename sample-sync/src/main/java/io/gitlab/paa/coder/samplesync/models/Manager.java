package io.gitlab.paa.coder.samplesync.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Cascade;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Data
@Table(name = "managers")
@Entity
@EqualsAndHashCode(exclude = "userInfo")
@ToString(exclude = "userInfo")
public class Manager {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "users_info_id",updatable = false,insertable = false)
    private UUID userInfoId;
    @OneToOne
    @Cascade({org.hibernate.annotations.CascadeType.PERSIST,org.hibernate.annotations.CascadeType.MERGE})
    @JoinColumn(name = "users_info_id", referencedColumnName = "id")
    @NotNull
    private UserInfo userInfo;

    @Column(name = "deleted_at")
    private Instant deletedAt;


    public void setUserInfo(UserInfo userInfo){
        Optional.ofNullable(userInfo).ifPresentOrElse(ui-> this.setUserInfoId(ui.getId()), ()->this.setUserInfoId(null));
        this.userInfo=userInfo;
    }

    public Boolean isActive(){
        return deletedAt==null;
    }

    public void toggleActivity(){
        setDeletedAt(isActive()?Instant.now():null);
    }
}

