package io.gitlab.paa.coder.samplesync;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class SampleSyncApplication {

    public static void main(String[] args){
        SpringApplication.run(SampleSyncApplication.class, args);
    }

}
