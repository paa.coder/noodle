package io.gitlab.paa.coder.samplesync.models;

import jakarta.persistence.*;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Optional;
import java.util.UUID;

@Data
@Table(name = "users_info")
@Entity
@EqualsAndHashCode(exclude = "manager")
@ToString(exclude = "manager")
public class UserInfo {

    @Id
    @NotNull
    private UUID id;

    @Valid
    @Embedded
    @NotNull
    public Properties props;

    @OneToOne(mappedBy = "userInfo")
    private Manager manager;

    @Data
    @Embeddable
    @Accessors(chain = true)
    public static class Properties{
        private String email;
        private String name;
        private String surname;
        private String patronymic;
    }

    public Optional<Manager> findManager(){
        return Optional.ofNullable(manager);
    }

}
