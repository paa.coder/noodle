package io.gitlab.paa.coder.samplesync.models.company;


import io.gitlab.paa.coder.samplesync.models.Manager;
import io.gitlab.paa.coder.samplesync.models.UserInfo;
import io.hypersistence.utils.hibernate.type.basic.PostgreSQLEnumType;
import jakarta.persistence.*;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Data
@Entity
@Table(name = "company_statements")
@EqualsAndHashCode(exclude = {"applicant", "manager", "company"})
@ToString(exclude = {"applicant", "manager", "company"})
public class CompanyStatement {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "company_statements_id_seq")
    @SequenceGenerator(name = "company_statements_id_seq", sequenceName = "company_statements_id_seq", allocationSize = 10)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "company_statements_status_enum")
    @Type(PostgreSQLEnumType.class)
    private Status status = Status.NEW;

    @Column(name = "applicant_id", updatable = false, insertable = false)
    private UUID applicantId;

    @NotNull
    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.PERSIST, org.hibernate.annotations.CascadeType.MERGE})
    @JoinColumn(name = "applicant_id", referencedColumnName = "id")
    private UserInfo applicant;


    @Column(name = "manager_id", updatable = false, insertable = false)
    private UUID managerId;

    @ManyToOne
    @JoinColumn(name = "manager_id", referencedColumnName = "id")
    private Manager manager;

    @NotNull
    @Valid
    private Properties properties;

    private String solution;

    @CreationTimestamp
    @Column(name = "created_at")
    private Instant createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    private Instant updatedAt;

    @OneToOne(mappedBy = "statement")
    private Company company;


    public void setApplicant(UserInfo applicant){
        Optional.ofNullable(applicant).ifPresentOrElse(ui -> this.setApplicantId(ui.getId()), () -> this.setApplicantId(null));
        this.applicant = applicant;
    }


    public void setManager(Manager manager){
        Optional.ofNullable(manager).ifPresentOrElse(ui -> this.setManagerId(ui.getId()), () -> this.setManagerId(null));
        this.manager = manager;
    }

    public enum Status {
        NEW,
        APPROVAL,
        REJECTED
    }

    @Data
    @Embeddable
    @Accessors(chain = true)
    public static class Properties {

        @NotNull
        private String inn;
        @NotNull
        private String ogrn;
        @NotNull
        @Column(name = "time_zone")
        private String timeZone;
        @NotBlank
        private String name;
    }
}
