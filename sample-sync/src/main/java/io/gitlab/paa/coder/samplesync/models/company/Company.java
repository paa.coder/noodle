package io.gitlab.paa.coder.samplesync.models.company;

import jakarta.persistence.*;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Optional;
import java.util.UUID;

@Data
@Entity
@Table(name = "companies")
@EqualsAndHashCode(exclude = "statement")
@ToString(exclude = "statement")
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;


    @Column(name = "statement_id", updatable = false, insertable = false)
    private Long statementId;

    @OneToOne
    @JoinColumn(name = "statement_id", referencedColumnName = "id", updatable = false)
    @NotNull
    private CompanyStatement statement;

    @NotNull
    @Valid
    private Properties properties;

    public Properties getProperties(){
        if(properties==null){
            properties = new Properties();
        }
        return properties;
    }

    public void setStatement(CompanyStatement statement){
        Optional.ofNullable(statement).ifPresentOrElse(i->setStatementId(i.getId()),()->setStatementId(null));
        this.statement = statement;
    }

    @Data
    @Embeddable
    @Accessors(chain = true)
    public static class Properties implements Cloneable {
        @NotBlank
        private String name;

        @Override
        public Properties clone(){
            try{
                return (Properties) super.clone();
            }catch(CloneNotSupportedException e){
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }
}
