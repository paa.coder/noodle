CREATE EXTENSION IF NOT EXISTS "uuid-ossp" schema public;

CREATE TABLE users_info
(
    id         uuid PRIMARY KEY,
    email      varchar(250),
    name       varchar(100),
    surname    varchar(100),
    patronymic varchar(100)
);

CREATE TABLE managers
(
    id            uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    users_info_id uuid not null unique,
    CONSTRAINT fk_users_info FOREIGN KEY (users_info_id) REFERENCES users_info (id) ON DELETE RESTRICT ON UPDATE CASCADE,
    deleted_at    timestamp(0) null
);

CREATE UNIQUE INDEX managers_unique_key ON managers (users_info_id) WHERE managers.deleted_at IS NULL;

CREATE SEQUENCE company_statements_id_seq increment 10;
CREATE TYPE company_statements_status_enum AS ENUM ('NEW','APPROVAL','REJECTED');

CREATE TABLE company_statements
(
    id            int8 PRIMARY KEY                        DEFAULT nextval('company_statements_id_seq'),
    status        company_statements_status_enum not null default 'NEW',

    applicant_id  uuid                           not null,
    CONSTRAINT fk_applicant FOREIGN KEY (applicant_id) REFERENCES users_info (id) ON DELETE RESTRICT ON UPDATE CASCADE,

    manager_id    uuid null,
    CONSTRAINT fk_managers FOREIGN KEY (manager_id) REFERENCES managers (id) ON DELETE RESTRICT ON UPDATE CASCADE,

    inn           varchar(12)                    not null,
    ogrn          varchar(15)                    not null,
    time_zone     varchar(100)                   not null,
    name          varchar(250)                   not null,

    solution      text,

    created_at    timestamp(0)                   not null DEFAULT NOW(),
    updated_at    timestamp(0)                   not null DEFAULT NOW()
);

CREATE SEQUENCE company_statement_comments_id_seq increment 10;

CREATE TABLE company_statement_comments
(
    id                   int8 PRIMARY KEY      DEFAULT nextval('company_statement_comments_id_seq'),

    manager_id           uuid         not null,
    CONSTRAINT fk_managers FOREIGN KEY (manager_id) REFERENCES managers (id) ON DELETE RESTRICT ON UPDATE CASCADE,

    company_statement_id int8         not null,
    CONSTRAINT fk_company_statements FOREIGN KEY (company_statement_id) REFERENCES company_statements (id) ON DELETE RESTRICT ON UPDATE CASCADE,

    message              text         not null,

    created_at           timestamp(0) not null DEFAULT NOW(),
    updated_at           timestamp(0) not null DEFAULT NOW()
);

CREATE TABLE companies
(
    id           uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),

    statement_id int8         not null,
    CONSTRAINT fk_statements FOREIGN KEY (statement_id) REFERENCES company_statements (id) ON DELETE RESTRICT ON UPDATE CASCADE,

    name         varchar(250) not null

);



